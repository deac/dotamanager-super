#pragma once

#include "../Serialise/Serialisable.h"
#include "Gender.h"
#include "Language.h"
#include <tuple>

class Names : Serialisable
{
public:
  void Serialise(Serialiser* serialiser)override;
  static Names* instance();
  std::pair<std::string,Gender> getRandomName(Language language);
  std::string getRandomTeamName(Language language);
private:
  Names();
  ~Names();
  int databaseIter[LanguageCount];
};
