#include "Date.h"

Date::Date()
{
  days = 0;
  months = 0;
  years = 0;
  dayOfTheWeek = 4;
}
void Date::Set(int _day, Month _month, int _year, int _dayOfTheWeek)
{
  days = _day;
  months = (int)_month;
  years = _year;
  dayOfTheWeek = _dayOfTheWeek;
}
void Date::Set(const Date& rhs)
{
  days = rhs.day();
  months = rhs.monthInt();
  years = rhs.year();
  dayOfTheWeek = rhs.getDayOfTheWeek();
}
/*bool Date::operator=(const Date& rhs)const
{
  return days == rhs.day() && months == rhs.monthInt() && years = rhs.year();
  }*/
bool Date::Equals(const Date& rhs)const
{
  return days == rhs.day() && months == rhs.monthInt() && years == rhs.year();
}
bool Date::operator>(const Date& rhs)const
{
  if (years > rhs.year()) return true;
  if (years < rhs.year()) return false;
  if (months > rhs.monthInt()) return true;
  if (months < rhs.monthInt()) return false;
  if (days > rhs.day()) return true;
  //if (days < rhs.day()) return false;
  return false;
}

void Date::Serialise(Serialiser* serialiser)
{
  serialiser->Template(&days);
  serialiser->Template(&months);
  serialiser->Template(&years);
  serialiser->Template(&dayOfTheWeek);
}
bool Date::isLeapYear(int _year)
{
  int year = _year + StartingYear;
  if (year % 4 != 0) return false;
  else if (year % 100 != 0) return true;
  else if (year % 400 != 0) return false;
  else return true;
}
Date::Month Date::month()const
{
  return static_cast<Date::Month>(months);
}
const char* Date::monthString()const
{
  switch (months)
    {
    case 0:
      return "January";
    case 1:
      return "February";
    case 2:
      return "March";
    case 3:
      return "April";
    case 4:
      return "May";
    case 5:
      return "June";
    case 6:
      return "July";
    case 7:
      return "August";
    case 8:
      return "September";
    case 9:
      return "October";
    case 10:
      return "November";
    case 11:
      return "December";
    default:
      std::cout << "Error: Invalid month " << months << std::endl;
      return nullptr;
    }
}
int Date::daysInMonth(int _month, int _year)
{
  switch (_month)
    {
    case 0:
      return 31;
    case 1:
      if (isLeapYear(_year))
	return 29;
      else
	return 28;
    case 2:
      return 31;
    case 3:
      return 30;
    case 4:
      return 31;
    case 5:
      return 30;
    case 6:
      return 31;
    case 7:
      return 31;
    case 8:
      return 30;
    case 9:
      return 31;
    case 10:
      return 30;
    case 11:
      return 31;
    default:
      std::cout << "Error: Invalid value for month " << _month << std::endl;
      return -1;
    }
}
int Date::integer()const
{
  return days + (months * 31) + (years * 31 * 12);
}
int Date::daysThisMonth()const
{
  return daysInMonth(months, years);
}
int Date::getDayOfFirstOfTheMonth()const
{
  int ret = getDayOfTheWeek() - day();
  while (ret < 0)
    ret += 7;
  return ret;
}
std::string Date::getString()const
{
  char buffer[64];
  sprintf(buffer, "%d-%d-%d", days+1, months+1, years);
  return buffer;
}
std::string Date::dateString(int _day)
{
  int day = _day;
  if (_day > 10 && _day < 14)
    day += 5;
  char buffer[5];
  switch (day % 10)
    {
    case 1:
      sprintf(buffer, "%dst", _day);
      break;
    case 2:
      sprintf(buffer, "%dnd", _day);
      break;
    case 3:
      sprintf(buffer, "%drd", _day);
      break;
    default:
      sprintf(buffer, "%dth", _day);
      break;
    };
  return buffer;
} 
void Date::operator+=(int _days)
{
  while (_days > 0)
    {
      increment();
      _days--;
    }
  while (_days < 0)
    {
      decrement();
      _days++;
    }
}
int Date::getDayOfTheWeek()const
{
  if (dayOfTheWeek == -1)
    std::cout << "Error: Day of the week not set correctly" << std::endl;
  return dayOfTheWeek;
}

void Date::increment()
{
  if (dayOfTheWeek != -1)
    {
      dayOfTheWeek++;
      if (dayOfTheWeek == 7)
	dayOfTheWeek = 0;
    }
  days++;
  if (days == daysInMonth(months, years))
    {
      days = 0;
      months++;
      if (months == 12)
	{
	  months = 0;
	  years++;
	}
    }
}
void Date::decrement()
{
  if (dayOfTheWeek != -1)
    {
      dayOfTheWeek--;
      if (dayOfTheWeek == -1)
	dayOfTheWeek = 6;
    }
  days--;
  if (days == -1)
    {
      months--;
      if (months == -1)
	{
	  months = 11;
	  years--;
	}
      days = daysInMonth(months, years)-1;
    }
}
Date Date::previousMonth()const
{
  Date ret = *this;
  int month = ret.months;
  while (month == ret.months)
    ret.decrement();
  return ret;
}
Date Date::nextMonth()const
{
  Date ret = *this;
  int month = ret.months;
  while (month == ret.months)
    ret.increment();
  return ret;
}

