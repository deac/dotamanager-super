#include "Regions.h"
#include <iostream>

Regions* Regions::instance()
{
  static Regions regions;
  return &regions;
}
std::string Regions::getName(Country country)
{
  return instance()->entries[country].countryName;
}
std::string Regions::getCurrencyCode(Country country)
{
  return instance()->entries[country].currencyCode;
}
std::string Regions::getCurrencyName(Country country)
{
  return instance()->entries[country].currencyName;
}
std::string Regions::getCurrencySymbol(Country country)
{
  return instance()->entries[country].currencySymbol;
}
std::string Regions::getFlag(Country country)
{
  std::string ret("Flags/");
  ret += instance()->entries[country].flagName;
  return ret;
}
std::string Regions::getContinent(Country country)
{
  switch (instance()->entries[country].continent)
    {
    case Africa:
      return "Africa";
    case Asia:
      return "Asia";
    case Europe:
      return "Europe";
    case NorthAmerica:
      return "NorthAmerica";
    case SouthAmerica:
      return "SouthAmerica";
    case Oceania:
      return "Oceania";
    case Antarctica_Continent:
      return "Antarctica";
    default:
      std::cout << "Error: Invalid continent " << instance()->entries[country].continent << std::endl;
      return "Error";
    }
}
unsigned int Regions::getIsoCode(Country country)
{
  return instance()->entries[country].isoCode;
}

void Regions::Set(const char* countryName, const char* isoAlpha2, const char* isoAlpha3, unsigned int isoCode, const char* currencyCode, const char* currencyName, const char* currencySymbol, const char* flagName)
{
  static int index = 0;
  entries[index].countryName = countryName;
  entries[index].isoAlpha2 = isoAlpha2;
  entries[index].isoAlpha3 = isoAlpha3;
  entries[index].isoCode = isoCode;
  entries[index].currencyCode = currencyCode;
  entries[index].currencyName = currencyName;
  entries[index].currencySymbol = currencySymbol;
  entries[index].flagName = flagName;
  entries[index].continent = ContinentError;
  index++;
}
void Regions::Set(Continent continent, unsigned int isoCode)
{
  unsigned int i = 0;
  for (i = 0; i != Size; i++)
    {
      if (entries[i].isoCode == isoCode)
	{
	  entries[i].continent = continent;
	  return;
	}
    }
  std::cout << "No country with iso code " << isoCode << std::endl;
}
void Regions::SetForAll(Continent continent, Language language)
{
  for (unsigned int i = 0; i != Size; i++)
    {
      if (entries[i].continent == continent)
	entries[i].language = language;
    }
}
void Regions::Set(std::vector<Country> countries, Language language)
{
  for (unsigned int i = 0; i != countries.size(); i++)
    entries[countries[i]].language = language;
}
Regions::Regions()
{
  // This first set of commands sets most of the values was downloaded from
  // http://www.techrecite.com/mysql-database-dump-for-countries-currency-and-flags/
  // as an SQL dump, and I ran various string replace commands to generate this code.
  
  // The second set of commands (below this first wall of commands) sets the continents and was downloaded from
  // http://en.wikipedia.org/wiki/List_of_sovereign_states_and_dependent_territories_by_continent_%28data_file%29
  // and I did something similar to generate them.

  // There were some discrepencies between the two data sets:
  // The continents data did not contain the country "Serbia and Montenegro" so I manually added it to europe
  // The continents data has 12 countries that are not in the first set and have such been commented out, these countries were:
  // 248, 499, 531, 534, 535, 652, 663, 688, 728, 831, 832, 833
  // So yea, the data is old (No North and South Sudan, only Sudan) but its the best that I could find.
  // Maybe if Scotland was independant I would care more about up to date geography #The45
  
  Set("Afghanistan", "AF", "AFG", 4, "AFN", "Afghani", "؋", "AF.png");
  Set("Albania", "AL", "ALB", 8, "ALL", "Lek", "Lek", "AL.png");
  Set("Algeria", "DZ", "DZA", 12, "DZD", "Dinar", NULL, "DZ.png");
  Set("American Samoa", "AS", "ASM", 16, "USD", "Dollar", "$", "AS.png");
  Set("Andorra", "AD", "AND", 20, "EUR", "Euro", "€", "AD.png");
  Set("Angola", "AO", "AGO", 24, "AOA", "Kwanza", "Kz", "AO.png");
  Set("Anguilla", "AI", "AIA", 660, "XCD", "Dollar", "$", "AI.png");
  Set("Antarctica", "AQ", "ATA", 10, "", "", NULL, "AQ.png");
  Set("Antigua and Barbuda", "AG", "ATG", 28, "XCD", "Dollar", "$", "AG.png");
  Set("Argentina", "AR", "ARG", 32, "ARS", "Peso", "$", "AR.png");
  Set("Armenia", "AM", "ARM", 51, "AMD", "Dram", NULL, "AM.png");
  Set("Aruba", "AW", "ABW", 533, "AWG", "Guilder", "ƒ", "AW.png");
  Set("Australia", "AU", "AUS", 36, "AUD", "Dollar", "$", "AU.png");
  Set("Austria", "AT", "AUT", 40, "EUR", "Euro", "€", "AT.png");
  Set("Azerbaijan", "AZ", "AZE", 31, "AZN", "Manat", "ман", "AZ.png");
  Set("Bahamas", "BS", "BHS", 44, "BSD", "Dollar", "$", "BS.png");
  Set("Bahrain", "BH", "BHR", 48, "BHD", "Dinar", NULL, "BH.png");
  Set("Bangladesh", "BD", "BGD", 50, "BDT", "Taka", NULL, "BD.png");
  Set("Barbados", "BB", "BRB", 52, "BBD", "Dollar", "$", "BB.png");
  Set("Belarus", "BY", "BLR", 112, "BYR", "Ruble", "p.", "BY.png");
  Set("Belgium", "BE", "BEL", 56, "EUR", "Euro", "€", "BE.png");
  Set("Belize", "BZ", "BLZ", 84, "BZD", "Dollar", "BZ$", "BZ.png");
  Set("Benin", "BJ", "BEN", 204, "XOF", "Franc", NULL, "BJ.png");
  Set("Bermuda", "BM", "BMU", 60, "BMD", "Dollar", "$", "BM.png");
  Set("Bhutan", "BT", "BTN", 64, "BTN", "Ngultrum", NULL, "BT.png");
  Set("Bolivia", "BO", "BOL", 68, "BOB", "Boliviano", "$b", "BO.png");
  Set("Bosnia and Herzegovina", "BA", "BIH", 70, "BAM", "Marka", "KM", "BA.png");
  Set("Botswana", "BW", "BWA", 72, "BWP", "Pula", "P", "BW.png");
  Set("Bouvet Island", "BV", "BVT", 74, "NOK", "Krone", "kr", "BV.png");
  Set("Brazil", "BR", "BRA", 76, "BRL", "Real", "R$", "BR.png");
  Set("British Indian Ocean Territory", "IO", "IOT", 86, "USD", "Dollar", "$", "IO.png");
  Set("British Virgin Islands", "VG", "VGB", 92, "USD", "Dollar", "$", "VG.png");
  Set("Brunei", "BN", "BRN", 96, "BND", "Dollar", "$", "BN.png");
  Set("Bulgaria", "BG", "BGR", 100, "BGN", "Lev", "лв", "BG.png");
  Set("Burkina Faso", "BF", "BFA", 854, "XOF", "Franc", NULL, "BF.png");
  Set("Burundi", "BI", "BDI", 108, "BIF", "Franc", NULL, "BI.png");
  Set("Cambodia", "KH", "KHM", 116, "KHR", "Riels", "៛", "KH.png");
  Set("Cameroon", "CM", "CMR", 120, "XAF", "Franc", "FCF", "CM.png");
  Set("Canada", "CA", "CAN", 124, "CAD", "Dollar", "$", "CA.png");
  Set("Cape Verde", "CV", "CPV", 132, "CVE", "Escudo", NULL, "CV.png");
  Set("Cayman Islands", "KY", "CYM", 136, "KYD", "Dollar", "$", "KY.png");
  Set("Central African Republic", "CF", "CAF", 140, "XAF", "Franc", "FCF", "CF.png");
  Set("Chad", "TD", "TCD", 148, "XAF", "Franc", NULL, "TD.png");
  Set("Chile", "CL", "CHL", 152, "CLP", "Peso", NULL, "CL.png");
  Set("China", "CN", "CHN", 156, "CNY", "Yuan Renminbi", "¥", "CN.png");
  Set("Christmas Island", "CX", "CXR", 162, "AUD", "Dollar", "$", "CX.png");
  Set("Cocos Islands", "CC", "CCK", 166, "AUD", "Dollar", "$", "CC.png");
  Set("Colombia", "CO", "COL", 170, "COP", "Peso", "$", "CO.png");
  Set("Comoros", "KM", "COM", 174, "KMF", "Franc", NULL, "KM.png");
  Set("Cook Islands", "CK", "COK", 184, "NZD", "Dollar", "$", "CK.png");
  Set("Costa Rica", "CR", "CRI", 188, "CRC", "Colon", "₡", "CR.png");
  Set("Croatia", "HR", "HRV", 191, "HRK", "Kuna", "kn", "HR.png");
  Set("Cuba", "CU", "CUB", 192, "CUP", "Peso", "₱", "CU.png");
  Set("Cyprus", "CY", "CYP", 196, "CYP", "Pound", NULL, "CY.png");
  Set("Czech Republic", "CZ", "CZE", 203, "CZK", "Koruna", "Kč", "CZ.png");
  Set("Democratic Republic of the Congo", "CD", "COD", 180, "CDF", "Franc", NULL, "CD.png");
  Set("Denmark", "DK", "DNK", 208, "DKK", "Krone", "kr", "DK.png");
  Set("Djibouti", "DJ", "DJI", 262, "DJF", "Franc", NULL, "DJ.png");
  Set("Dominica", "DM", "DMA", 212, "XCD", "Dollar", "$", "DM.png");
  Set("Dominican Republic", "DO", "DOM", 214, "DOP", "Peso", "RD$", "DO.png");
  Set("East Timor", "TL", "TLS", 626, "USD", "Dollar", "$", "TL.png");
  Set("Ecuador", "EC", "ECU", 218, "USD", "Dollar", "$", "EC.png");
  Set("Egypt", "EG", "EGY", 818, "EGP", "Pound", "£", "EG.png");
  Set("El Salvador", "SV", "SLV", 222, "SVC", "Colone", "$", "SV.png");
  Set("Equatorial Guinea", "GQ", "GNQ", 226, "XAF", "Franc", "FCF", "GQ.png");
  Set("Eritrea", "ER", "ERI", 232, "ERN", "Nakfa", "Nfk", "ER.png");
  Set("Estonia", "EE", "EST", 233, "EEK", "Kroon", "kr", "EE.png");
  Set("Ethiopia", "ET", "ETH", 231, "ETB", "Birr", NULL, "ET.png");
  Set("Falkland Islands", "FK", "FLK", 238, "FKP", "Pound", "£", "FK.png");
  Set("Faroe Islands", "FO", "FRO", 234, "DKK", "Krone", "kr", "FO.png");
  Set("Fiji", "FJ", "FJI", 242, "FJD", "Dollar", "$", "FJ.png");
  Set("Finland", "FI", "FIN", 246, "EUR", "Euro", "€", "FI.png");
  Set("France", "FR", "FRA", 250, "EUR", "Euro", "€", "FR.png");
  Set("French Guiana", "GF", "GUF", 254, "EUR", "Euro", "€", "GF.png");
  Set("French Polynesia", "PF", "PYF", 258, "XPF", "Franc", NULL, "PF.png");
  Set("French Southern Territories", "TF", "ATF", 260, "EUR", "Euro  ", "€", "TF.png");
  Set("Gabon", "GA", "GAB", 266, "XAF", "Franc", "FCF", "GA.png");
  Set("Gambia", "GM", "GMB", 270, "GMD", "Dalasi", "D", "GM.png");
  Set("Georgia", "GE", "GEO", 268, "GEL", "Lari", NULL, "GE.png");
  Set("Germany", "DE", "DEU", 276, "EUR", "Euro", "€", "DE.png");
  Set("Ghana", "GH", "GHA", 288, "GHC", "Cedi", "¢", "GH.png");
  Set("Gibraltar", "GI", "GIB", 292, "GIP", "Pound", "£", "GI.png");
  Set("Greece", "GR", "GRC", 300, "EUR", "Euro", "€", "GR.png");
  Set("Greenland", "GL", "GRL", 304, "DKK", "Krone", "kr", "GL.png");
  Set("Grenada", "GD", "GRD", 308, "XCD", "Dollar", "$", "GD.png");
  Set("Guadeloupe", "GP", "GLP", 312, "EUR", "Euro", "€", "GP.png");
  Set("Guam", "GU", "GUM", 316, "USD", "Dollar", "$", "GU.png");
  Set("Guatemala", "GT", "GTM", 320, "GTQ", "Quetzal", "Q", "GT.png");
  Set("Guinea", "GN", "GIN", 324, "GNF", "Franc", NULL, "GN.png");
  Set("Guinea-Bissau", "GW", "GNB", 624, "XOF", "Franc", NULL, "GW.png");
  Set("Guyana", "GY", "GUY", 328, "GYD", "Dollar", "$", "GY.png");
  Set("Haiti", "HT", "HTI", 332, "HTG", "Gourde", "G", "HT.png");
  Set("Heard Island and McDonald Islands", "HM", "HMD", 334, "AUD", "Dollar", "$", "HM.png");
  Set("Honduras", "HN", "HND", 340, "HNL", "Lempira", "L", "HN.png");
  Set("Hong Kong", "HK", "HKG", 344, "HKD", "Dollar", "$", "HK.png");
  Set("Hungary", "HU", "HUN", 348, "HUF", "Forint", "Ft", "HU.png");
  Set("Iceland", "IS", "ISL", 352, "ISK", "Krona", "kr", "IS.png");
  Set("India", "IN", "IND", 356, "INR", "Rupee", "₹", "IN.png");
  Set("Indonesia", "ID", "IDN", 360, "IDR", "Rupiah", "Rp", "ID.png");
  Set("Iran", "IR", "IRN", 364, "IRR", "Rial", "﷼", "IR.png");
  Set("Iraq", "IQ", "IRQ", 368, "IQD", "Dinar", NULL, "IQ.png");
  Set("Ireland", "IE", "IRL", 372, "EUR", "Euro", "€", "IE.png");
  Set("Israel", "IL", "ISR", 376, "ILS", "Shekel", "₪", "IL.png");
  Set("Italy", "IT", "ITA", 380, "EUR", "Euro", "€", "IT.png");
  Set("Ivory Coast", "CI", "CIV", 384, "XOF", "Franc", NULL, "CI.png");
  Set("Jamaica", "JM", "JAM", 388, "JMD", "Dollar", "$", "JM.png");
  Set("Japan", "JP", "JPN", 392, "JPY", "Yen", "¥", "JP.png");
  Set("Jordan", "JO", "JOR", 400, "JOD", "Dinar", NULL, "JO.png");
  Set("Kazakhstan", "KZ", "KAZ", 398, "KZT", "Tenge", "лв", "KZ.png");
  Set("Kenya", "KE", "KEN", 404, "KES", "Shilling", NULL, "KE.png");
  Set("Kiribati", "KI", "KIR", 296, "AUD", "Dollar", "$", "KI.png");
  Set("Kuwait", "KW", "KWT", 414, "KWD", "Dinar", NULL, "KW.png");
  Set("Kyrgyzstan", "KG", "KGZ", 417, "KGS", "Som", "лв", "KG.png");
  Set("Laos", "LA", "LAO", 418, "LAK", "Kip", "₭", "LA.png");
  Set("Latvia", "LV", "LVA", 428, "LVL", "Lat", "Ls", "LV.png");
  Set("Lebanon", "LB", "LBN", 422, "LBP", "Pound", "£", "LB.png");
  Set("Lesotho", "LS", "LSO", 426, "LSL", "Loti", "L", "LS.png");
  Set("Liberia", "LR", "LBR", 430, "LRD", "Dollar", "$", "LR.png");
  Set("Libya", "LY", "LBY", 434, "LYD", "Dinar", NULL, "LY.png");
  Set("Liechtenstein", "LI", "LIE", 438, "CHF", "Franc", "CHF", "LI.png");
  Set("Lithuania", "LT", "LTU", 440, "LTL", "Litas", "Lt", "LT.png");
  Set("Luxembourg", "LU", "LUX", 442, "EUR", "Euro", "€", "LU.png");
  Set("Macao", "MO", "MAC", 446, "MOP", "Pataca", "MOP", "MO.png");
  Set("Macedonia", "MK", "MKD", 807, "MKD", "Denar", "ден", "MK.png");
  Set("Madagascar", "MG", "MDG", 450, "MGA", "Ariary", NULL, "MG.png");
  Set("Malawi", "MW", "MWI", 454, "MWK", "Kwacha", "MK", "MW.png");
  Set("Malaysia", "MY", "MYS", 458, "MYR", "Ringgit", "RM", "MY.png");
  Set("Maldives", "MV", "MDV", 462, "MVR", "Rufiyaa", "Rf", "MV.png");
  Set("Mali", "ML", "MLI", 466, "XOF", "Franc", NULL, "ML.png");
  Set("Malta", "MT", "MLT", 470, "MTL", "Lira", NULL, "MT.png");
  Set("Marshall Islands", "MH", "MHL", 584, "USD", "Dollar", "$", "MH.png");
  Set("Martinique", "MQ", "MTQ", 474, "EUR", "Euro", "€", "MQ.png");
  Set("Mauritania", "MR", "MRT", 478, "MRO", "Ouguiya", "UM", "MR.png");
  Set("Mauritius", "MU", "MUS", 480, "MUR", "Rupee", "₨", "MU.png");
  Set("Mayotte", "YT", "MYT", 175, "EUR", "Euro", "€", "YT.png");
  Set("Mexico", "MX", "MEX", 484, "MXN", "Peso", "$", "MX.png");
  Set("Micronesia", "FM", "FSM", 583, "USD", "Dollar", "$", "FM.png");
  Set("Moldova", "MD", "MDA", 498, "MDL", "Leu", NULL, "MD.png");
  Set("Monaco", "MC", "MCO", 492, "EUR", "Euro", "€", "MC.png");
  Set("Mongolia", "MN", "MNG", 496, "MNT", "Tugrik", "₮", "MN.png");
  Set("Montserrat", "MS", "MSR", 500, "XCD", "Dollar", "$", "MS.png");
  Set("Morocco", "MA", "MAR", 504, "MAD", "Dirham", NULL, "MA.png");
  Set("Mozambique", "MZ", "MOZ", 508, "MZN", "Meticail", "MT", "MZ.png");
  Set("Myanmar", "MM", "MMR", 104, "MMK", "Kyat", "K", "MM.png");
  Set("Namibia", "NA", "NAM", 516, "NAD", "Dollar", "$", "NA.png");
  Set("Nauru", "NR", "NRU", 520, "AUD", "Dollar", "$", "NR.png");
  Set("Nepal", "NP", "NPL", 524, "NPR", "Rupee", "₨", "NP.png");
  Set("Netherlands", "NL", "NLD", 528, "EUR", "Euro", "€", "NL.png");
  Set("Netherlands Antilles", "AN", "ANT", 530, "ANG", "Guilder", "ƒ", "AN.png");
  Set("New Caledonia", "NC", "NCL", 540, "XPF", "Franc", NULL, "NC.png");
  Set("New Zealand", "NZ", "NZL", 554, "NZD", "Dollar", "$", "NZ.png");
  Set("Nicaragua", "NI", "NIC", 558, "NIO", "Cordoba", "C$", "NI.png");
  Set("Niger", "NE", "NER", 562, "XOF", "Franc", NULL, "NE.png");
  Set("Nigeria", "NG", "NGA", 566, "NGN", "Naira", "₦", "NG.png");
  Set("Niue", "NU", "NIU", 570, "NZD", "Dollar", "$", "NU.png");
  Set("Norfolk Island", "NF", "NFK", 574, "AUD", "Dollar", "$", "NF.png");
  Set("North Korea", "KP", "PRK", 408, "KPW", "Won", "₩", "KP.png");
  Set("Northern Mariana Islands", "MP", "MNP", 580, "USD", "Dollar", "$", "MP.png");
  Set("Norway", "NO", "NOR", 578, "NOK", "Krone", "kr", "NO.png");
  Set("Oman", "OM", "OMN", 512, "OMR", "Rial", "﷼", "OM.png");
  Set("Pakistan", "PK", "PAK", 586, "PKR", "Rupee", "₨", "PK.png");
  Set("Palau", "PW", "PLW", 585, "USD", "Dollar", "$", "PW.png");
  Set("Palestinian Territory", "PS", "PSE", 275, "ILS", "Shekel", "₪", "PS.png");
  Set("Panama", "PA", "PAN", 591, "PAB", "Balboa", "B/.", "PA.png");
  Set("Papua New Guinea", "PG", "PNG", 598, "PGK", "Kina", NULL, "PG.png");
  Set("Paraguay", "PY", "PRY", 600, "PYG", "Guarani", "Gs", "PY.png");
  Set("Peru", "PE", "PER", 604, "PEN", "Sol", "S/.", "PE.png");
  Set("Philippines", "PH", "PHL", 608, "PHP", "Peso", "Php", "PH.png");
  Set("Pitcairn", "PN", "PCN", 612, "NZD", "Dollar", "$", "PN.png");
  Set("Poland", "PL", "POL", 616, "PLN", "Zloty", "zł", "PL.png");
  Set("Portugal", "PT", "PRT", 620, "EUR", "Euro", "€", "PT.png");
  Set("Puerto Rico", "PR", "PRI", 630, "USD", "Dollar", "$", "PR.png");
  Set("Qatar", "QA", "QAT", 634, "QAR", "Rial", "﷼", "QA.png");
  Set("Republic of the Congo", "CG", "COG", 178, "XAF", "Franc", "FCF", "CG.png");
  Set("Reunion", "RE", "REU", 638, "EUR", "Euro", "€", "RE.png");
  Set("Romania", "RO", "ROU", 642, "RON", "Leu", "lei", "RO.png");
  Set("Russia", "RU", "RUS", 643, "RUB", "Ruble", "руб", "RU.png");
  Set("Rwanda", "RW", "RWA", 646, "RWF", "Franc", NULL, "RW.png");
  Set("Saint Helena", "SH", "SHN", 654, "SHP", "Pound", "£", "SH.png");
  Set("Saint Kitts and Nevis", "KN", "KNA", 659, "XCD", "Dollar", "$", "KN.png");
  Set("Saint Lucia", "LC", "LCA", 662, "XCD", "Dollar", "$", "LC.png");
  Set("Saint Pierre and Miquelon", "PM", "SPM", 666, "EUR", "Euro", "€", "PM.png");
  Set("Saint Vincent and the Grenadines", "VC", "VCT", 670, "XCD", "Dollar", "$", "VC.png");
  Set("Samoa", "WS", "WSM", 882, "WST", "Tala", "WS$", "WS.png");
  Set("San Marino", "SM", "SMR", 674, "EUR", "Euro", "€", "SM.png");
  Set("Sao Tome and Principe", "ST", "STP", 678, "STD", "Dobra", "Db", "ST.png");
  Set("Saudi Arabia", "SA", "SAU", 682, "SAR", "Rial", "﷼", "SA.png");
  Set("Senegal", "SN", "SEN", 686, "XOF", "Franc", NULL, "SN.png");
  Set("Serbia and Montenegro", "CS", "SCG", 891, "RSD", "Dinar", "Дин", "CS.png");
  Set("Seychelles", "SC", "SYC", 690, "SCR", "Rupee", "₨", "SC.png");
  Set("Sierra Leone", "SL", "SLE", 694, "SLL", "Leone", "Le", "SL.png");
  Set("Singapore", "SG", "SGP", 702, "SGD", "Dollar", "$", "SG.png");
  Set("Slovakia", "SK", "SVK", 703, "SKK", "Koruna", "Sk", "SK.png");
  Set("Slovenia", "SI", "SVN", 705, "EUR", "Euro", "€", "SI.png");
  Set("Solomon Islands", "SB", "SLB", 90, "SBD", "Dollar", "$", "SB.png");
  Set("Somalia", "SO", "SOM", 706, "SOS", "Shilling", "S", "SO.png");
  Set("South Africa", "ZA", "ZAF", 710, "ZAR", "Rand", "R", "ZA.png");
  Set("South Georgia and the South Sandwich Islands", "GS", "SGS", 239, "GBP", "Pound", "£", "GS.png");
  Set("South Korea", "KR", "KOR", 410, "KRW", "Won", "₩", "KR.png");
  Set("Spain", "ES", "ESP", 724, "EUR", "Euro", "€", "ES.png");
  Set("Sri Lanka", "LK", "LKA", 144, "LKR", "Rupee", "₨", "LK.png");
  Set("Sudan", "SD", "SDN", 736, "SDD", "Dinar", NULL, "SD.png");
  Set("Suriname", "SR", "SUR", 740, "SRD", "Dollar", "$", "SR.png");
  Set("Svalbard and Jan Mayen", "SJ", "SJM", 744, "NOK", "Krone", "kr", "SJ.png");
  Set("Swaziland", "SZ", "SWZ", 748, "SZL", "Lilangeni", NULL, "SZ.png");
  Set("Sweden", "SE", "SWE", 752, "SEK", "Krona", "kr", "SE.png");
  Set("Switzerland", "CH", "CHE", 756, "CHF", "Franc", "CHF", "CH.png");
  Set("Syria", "SY", "SYR", 760, "SYP", "Pound", "£", "SY.png");
  Set("Taiwan", "TW", "TWN", 158, "TWD", "Dollar", "NT$", "TW.png");
  Set("Tajikistan", "TJ", "TJK", 762, "TJS", "Somoni", NULL, "TJ.png");
  Set("Tanzania", "TZ", "TZA", 834, "TZS", "Shilling", NULL, "TZ.png");
  Set("Thailand", "TH", "THA", 764, "THB", "Baht", "฿", "TH.png");
  Set("Togo", "TG", "TGO", 768, "XOF", "Franc", NULL, "TG.png");
  Set("Tokelau", "TK", "TKL", 772, "NZD", "Dollar", "$", "TK.png");
  Set("Tonga", "TO", "TON", 776, "TOP", "Pa""anga", "T$", "TO.png");
  Set("Trinidad and Tobago", "TT", "TTO", 780, "TTD", "Dollar", "TT$", "TT.png");
  Set("Tunisia", "TN", "TUN", 788, "TND", "Dinar", NULL, "TN.png");
  Set("Turkey", "TR", "TUR", 792, "TRY", "Lira", "YTL", "TR.png");
  Set("Turkmenistan", "TM", "TKM", 795, "TMM", "Manat", "m", "TM.png");
  Set("Turks and Caicos Islands", "TC", "TCA", 796, "USD", "Dollar", "$", "TC.png");
  Set("Tuvalu", "TV", "TUV", 798, "AUD", "Dollar", "$", "TV.png");
  Set("U.S. Virgin Islands", "VI", "VIR", 850, "USD", "Dollar", "$", "VI.png");
  Set("Uganda", "UG", "UGA", 800, "UGX", "Shilling", NULL, "UG.png");
  Set("Ukraine", "UA", "UKR", 804, "UAH", "Hryvnia", "₴", "UA.png");
  Set("United Arab Emirates", "AE", "ARE", 784, "AED", "Dirham", NULL, "AE.png");
  Set("United Kingdom", "GB", "GBR", 826, "GBP", "Pound", "£", "GB.png");
  Set("United States", "US", "USA", 840, "USD", "Dollar", "$", "US.png");
  Set("United States Minor Outlying Islands", "UM", "UMI", 581, "USD", "Dollar ", "$", "UM.png");
  Set("Uruguay", "UY", "URY", 858, "UYU", "Peso", "$U", "UY.png");
  Set("Uzbekistan", "UZ", "UZB", 860, "UZS", "Som", "лв", "UZ.png");
  Set("Vanuatu", "VU", "VUT", 548, "VUV", "Vatu", "Vt", "VU.png");
  Set("Vatican", "VA", "VAT", 336, "EUR", "Euro", "€", "VA.png");
  Set("Venezuela", "VE", "VEN", 862, "VEF", "Bolivar", "Bs", "VE.png");
  Set("Vietnam", "VN", "VNM", 704, "VND", "Dong", "₫", "VN.png");
  Set("Wallis and Futuna", "WF", "WLF", 876, "XPF", "Franc", NULL, "WF.png");
  Set("Western Sahara", "EH", "ESH", 732, "MAD", "Dirham", NULL, "EH.png");
  Set("Yemen", "YE", "YEM", 887, "YER", "Rial", "﷼", "YE.png");
  Set("Zambia", "ZM", "ZMB", 894, "ZMK", "Kwacha", "ZK", "ZM.png");
  Set("Zimbabwe", "ZW", "ZWE", 716, "ZWD", "Dollar", "Z$", "ZW.png");
  Set(Asia, 4);
  Set(Europe, 8);
  Set(Antarctica_Continent, 10);
  Set(Africa, 12);
  Set(Oceania, 16);
  Set(Europe, 20);
  Set(Africa, 24);
  Set(NorthAmerica, 28);
  Set(Europe, 31);
  Set(Asia, 31);
  Set(SouthAmerica, 32);
  Set(Oceania, 36);
  Set(Europe, 40);
  Set(NorthAmerica, 44);
  Set(Asia, 48);
  Set(Asia, 50);
  Set(Europe, 51);
  Set(Asia, 51);
  Set(NorthAmerica, 52);
  Set(Europe, 56);
  Set(NorthAmerica, 60);
  Set(Asia, 64);
  Set(SouthAmerica, 68);
  Set(Europe, 70);
  Set(Africa, 72);
  Set(Antarctica_Continent, 74);
  Set(SouthAmerica, 76);
  Set(NorthAmerica, 84);
  Set(Asia, 86);
  Set(Oceania, 90);
  Set(NorthAmerica, 92);
  Set(Asia, 96);
  Set(Europe, 100);
  Set(Asia, 104);
  Set(Africa, 108);
  Set(Europe, 112);
  Set(Asia, 116);
  Set(Africa, 120);
  Set(NorthAmerica, 124);
  Set(Africa, 132);
  Set(NorthAmerica, 136);
  Set(Africa, 140);
  Set(Asia, 144);
  Set(Africa, 148);
  Set(SouthAmerica, 152);
  Set(Asia, 156);
  Set(Asia, 158);
  Set(Asia, 162);
  Set(Asia, 166);
  Set(SouthAmerica, 170);
  Set(Africa, 174);
  Set(Africa, 175);
  Set(Africa, 178);
  Set(Africa, 180);
  Set(Oceania, 184);
  Set(NorthAmerica, 188);
  Set(Europe, 191);
  Set(NorthAmerica, 192);
  Set(Europe, 196);
  Set(Asia, 196);
  Set(Europe, 203);
  Set(Africa, 204);
  Set(Europe, 208);
  Set(NorthAmerica, 212);
  Set(NorthAmerica, 214);
  Set(SouthAmerica, 218);
  Set(NorthAmerica, 222);
  Set(Africa, 226);
  Set(Africa, 231);
  Set(Africa, 232);
  Set(Europe, 233);
  Set(Europe, 234);
  Set(SouthAmerica, 238);
  Set(Antarctica_Continent, 239);
  Set(Oceania, 242);
  Set(Europe, 246);
  //Set(Europe, 248);
  Set(Europe, 250);
  Set(SouthAmerica, 254);
  Set(Oceania, 258);
  Set(Antarctica_Continent, 260);
  Set(Africa, 262);
  Set(Africa, 266);
  Set(Europe, 268);
  Set(Asia, 268);
  Set(Africa, 270);
  Set(Asia, 275);
  Set(Europe, 276);
  Set(Africa, 288);
  Set(Europe, 292);
  Set(Oceania, 296);
  Set(Europe, 300);
  Set(NorthAmerica, 304);
  Set(NorthAmerica, 308);
  Set(NorthAmerica, 312);
  Set(Oceania, 316);
  Set(NorthAmerica, 320);
  Set(Africa, 324);
  Set(SouthAmerica, 328);
  Set(NorthAmerica, 332);
  Set(Antarctica_Continent, 334);
  Set(Europe, 336);
  Set(NorthAmerica, 340);
  Set(Asia, 344);
  Set(Europe, 348);
  Set(Europe, 352);
  Set(Asia, 356);
  Set(Asia, 360);
  Set(Asia, 364);
  Set(Asia, 368);
  Set(Europe, 372);
  Set(Asia, 376);
  Set(Europe, 380);
  Set(Africa, 384);
  Set(NorthAmerica, 388);
  Set(Asia, 392);
  Set(Europe, 398);
  Set(Asia, 398);
  Set(Asia, 400);
  Set(Africa, 404);
  Set(Asia, 408);
  Set(Asia, 410);
  Set(Asia, 414);
  Set(Asia, 417);
  Set(Asia, 418);
  Set(Asia, 422);
  Set(Africa, 426);
  Set(Europe, 428);
  Set(Africa, 430);
  Set(Africa, 434);
  Set(Europe, 438);
  Set(Europe, 440);
  Set(Europe, 442);
  Set(Asia, 446);
  Set(Africa, 450);
  Set(Africa, 454);
  Set(Asia, 458);
  Set(Asia, 462);
  Set(Africa, 466);
  Set(Europe, 470);
  Set(NorthAmerica, 474);
  Set(Africa, 478);
  Set(Africa, 480);
  Set(NorthAmerica, 484);
  Set(Europe, 492);
  Set(Asia, 496);
  Set(Europe, 498);
  //Set(Europe, 499);
  Set(NorthAmerica, 500);
  Set(Africa, 504);
  Set(Africa, 508);
  Set(Asia, 512);
  Set(Africa, 516);
  Set(Oceania, 520);
  Set(Asia, 524);
  Set(Europe, 528);
  Set(NorthAmerica, 530);
  //Set(NorthAmerica, 531);
  Set(NorthAmerica, 533);
  //Set(NorthAmerica, 534);
  //Set(NorthAmerica, 535);
  Set(Oceania, 540);
  Set(Oceania, 548);
  Set(Oceania, 554);
  Set(NorthAmerica, 558);
  Set(Africa, 562);
  Set(Africa, 566);
  Set(Oceania, 570);
  Set(Oceania, 574);
  Set(Europe, 578);
  Set(Oceania, 580);
  Set(Oceania, 581);
  Set(NorthAmerica, 581);
  Set(Oceania, 583);
  Set(Oceania, 584);
  Set(Oceania, 585);
  Set(Asia, 586);
  Set(NorthAmerica, 591);
  Set(Oceania, 598);
  Set(SouthAmerica, 600);
  Set(SouthAmerica, 604);
  Set(Asia, 608);
  Set(Oceania, 612);
  Set(Europe, 616);
  Set(Europe, 620);
  Set(Africa, 624);
  Set(Asia, 626);
  Set(NorthAmerica, 630);
  Set(Asia, 634);
  Set(Africa, 638);
  Set(Europe, 642);
  Set(Europe, 643);
  Set(Asia, 643);
  Set(Africa, 646);
  //Set(NorthAmerica, 652);
  Set(Africa, 654);
  Set(NorthAmerica, 659);
  Set(NorthAmerica, 660);
  Set(NorthAmerica, 662);
  //Set(NorthAmerica, 663);
  Set(NorthAmerica, 666);
  Set(NorthAmerica, 670);
  Set(Europe, 674);
  Set(Africa, 678);
  Set(Asia, 682);
  Set(Africa, 686);
  //Set(Europe, 688);
  Set(Africa, 690);
  Set(Africa, 694);
  Set(Asia, 702);
  Set(Europe, 703);
  Set(Asia, 704);
  Set(Europe, 705);
  Set(Africa, 706);
  Set(Africa, 710);
  Set(Africa, 716);
  Set(Europe, 724);
  //Set(Africa, 728);
  Set(Africa, 732);
  Set(Africa, 736);
  Set(SouthAmerica, 740);
  Set(Europe, 744);
  Set(Africa, 748);
  Set(Europe, 752);
  Set(Europe, 756);
  Set(Asia, 760);
  Set(Asia, 762);
  Set(Asia, 764);
  Set(Africa, 768);
  Set(Oceania, 772);
  Set(Oceania, 776);
  Set(NorthAmerica, 780);
  Set(Asia, 784);
  Set(Africa, 788);
  Set(Europe, 792);
  Set(Asia, 792);
  Set(Asia, 795);
  Set(NorthAmerica, 796);
  Set(Oceania, 798);
  Set(Africa, 800);
  Set(Europe, 804);
  Set(Europe, 807);
  Set(Africa, 818);
  Set(Europe, 826);
  //Set(Europe, 831);
  //Set(Europe, 832);
  //Set(Europe, 833);
  Set(Africa, 834);
  Set(NorthAmerica, 840);
  Set(NorthAmerica, 850);
  Set(Africa, 854);
  Set(SouthAmerica, 858);
  Set(Asia, 860);
  Set(SouthAmerica, 862);
  Set(Oceania, 876);
  Set(Oceania, 882);
  Set(Asia, 887);
  Set(Europe, 891);
  Set(Africa, 894);
  for (unsigned int i = 0; i != Size; i++)
    {
      if (entries[i].continent == ContinentError)
	std::cout << "No continent for " << entries[i].countryName << std::endl;
    }
  
  // The most racist code I have ever written.
  
  // Notably poorly classified countries: India, pakistan, korea, most ex-soviet states, all of africa, I have probably gotten a fair few south and central american countries wrong, I lumped a lot of western european countries together, all of SEA except thailand and vietnam.
  SetForAll(Africa, African);
  SetForAll(Asia, Arabic); 
  SetForAll(Europe, British);
  SetForAll(NorthAmerica, American);
  SetForAll(SouthAmerica, Hispanic);
  SetForAll(Oceania, Australian);
  SetForAll(Antarctica_Continent, British);

  Set({China, Taiwan, HongKong}, Chinese);
  Set({Finland, Hungary}, Finnish);
  Set({France}, French);
  Set({Netherlands, NetherlandsAntilles, Germany}, Germanic);

  Set({Spain, Portugal, Andorra, TrinidadAndTobago, Mexico, Belize, CostaRica, ElSalvador, Guatemala, Honduras, Nicaragua, Panama}, Hispanic);
  
  Set({Greenland, Iceland}, Icelandic);
  Set({Italy, Greece}, Italian);
  Set({Brazil}, Brazilian);
  Set({Japan}, Japanese);
  Set({Denmark, Norway, Sweden}, Nordic);
  //Set({Afghanistan,Tajikistan,Uzbekistan,Kuwait,Bahrain,Azerbaijan}, Persian);
  Set({Armenia,Belarus,Estonia,Georgia,Kazakhstan,Kyrgyzstan,Latvia,Lithuania,Moldova,Russia,Tajikistan,Turkmenistan,Ukraine, Poland}, Russian);
  Set({Thailand}, Thai);
  Set({Vietnam, Philippines, Malaysia, EastTimor, Indonesia, Brunei,Singapore,Cambodia, Laos, Myanmar, Thailand}, Vietnamese);
}
Regions::~Regions()
{
}
