#pragma once

#include <string>
#include <vector>
#include "Country.h"
#include "Language.h"

class Regions
{
public:
  static std::string getName(Country country); /// User string, "United Kingdom"
  static std::string getCurrencyCode(Country country); /// Identifier, "GBP"
  static std::string getCurrencyName(Country country); /// User string, "Pound"
  static std::string getCurrencySymbol(Country country); /// Ascii symbol, "£"
  static std::string getFlag(Country country); /// Filename for cocos, "Flags/GB.png"
  static std::string getContinent(Country country); /// User string, "Europe"
  static unsigned int getIsoCode(Country country); /// Codes defined by ISO 3166-1, 826
  static const unsigned int Size = 239;
private:
  static Regions* instance();
  Regions();
  ~Regions();
  struct Entry
  {
    const char* countryName;
    const char* isoAlpha2;
    const char* isoAlpha3;
    unsigned int isoCode;
    const char* currencyCode;
    const char* currencyName;
    const char* currencySymbol;
    const char* flagName;
    Continent continent;
    Language language;
  };
  Entry entries[Size];
  void Set(const char* countryName, const char* isoAlpha2, const char* isoAlpha3, unsigned int isoCode, const char* currencyCode, const char* currencyName, const char* currencySymbol, const char* flagName);
  void Set(Continent continent, unsigned int isoCode);
  void SetForAll(Continent continent, Language language);
  void Set(std::vector<Country> countries, Language language);
};
