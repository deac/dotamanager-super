#pragma once
#include "../Serialise/Serialisable.h"

class Date : public Serialisable
{
 public:
  Date();
  int day()const{return days;}
  enum Month
    {
      Jan = 0,
      Feb,
      Mar,
      Apr,
      May,
      Jun,
      Jul,
      Aug,
      Sep,
      Oct,
      Nov,
      Dec,
    };
  void Set(int _day, Month _month, int _year, int _dayOfTheWeek);
  void Set(const Date& rhs);
  Month month()const;
  const char* monthString()const;
  void Serialise(Serialiser* serialiser)override;
  int year()const{return years;}
  int monthInt()const{return months;}
  int daysThisMonth()const;
  int integer()const;
  // 0 = Monday 6 = Sunday
  int getDayOfTheWeek()const;
  int getDayOfFirstOfTheMonth()const;
  int operator()()const{return integer();}
  void operator+=(int _days);
  void operator++(int i){increment();}
  void operator--(int i){decrement();}
  bool operator==(const Date& rhs)const;
  bool operator >(const Date& rhs)const;
  bool Equals(const Date& rhs)const;
  std::string getString()const;
  static std::string dateString(int _day);
  Date previousMonth()const;
  Date nextMonth()const;
 private:
  void increment();
  void decrement();
  int years; /// # of years since StartingYear
  int months;
  int days;
  int dayOfTheWeek;
  static const int StartingYear = 1960;
  static bool isLeapYear(int _year);
  static int daysInMonth(int _month, int _year);
};
