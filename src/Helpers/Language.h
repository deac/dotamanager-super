#pragma once

enum Language
  {
    LanguageError,
    African,
    American,
    Arabic,
    Australian,
    Brazilian,
    British,
    Chinese,
    Finnish,
    French,
    Germanic,
    Hispanic,
    Icelandic,
    Italian,
    Japanese,
    Nordic,
    Russian,
    Thai,
    Vietnamese,
  };
static const unsigned int LanguageCount = 19;
