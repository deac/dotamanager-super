#include "Names.h"
#include <iostream>
#include <fstream>
#include <cocos2d.h>
#include <stdlib.h>

Names* Names::instance()
{
  static Names names;
  return &names;
}

Names::Names()
{
  for (unsigned int i = 0; i != LanguageCount; i++)
    {
      databaseIter[i] = -1;
    }
}
Names::~Names()
{
}
void Names::Serialise(Serialiser* serialiser)
{
  for (unsigned int i = 0; i != LanguageCount; i++)
    {
      int* ptr = databaseIter + i;
      serialiser->Template(ptr);
    }
}
std::pair<std::string, Gender> Names::getRandomName(Language language)
{
  std::pair<std::string, Gender> ret;
  std::string filename;
  switch (language)
    {
    case African:
      filename = "African";
      break;
    case American:
      filename = "American";
      break;
    case Arabic:
      filename = "Arabic";
      break;
    case Australian:
      filename = "Australian";
      break;
    case Brazilian:
      filename = "Brazilian";
      break;
    case British:
      filename = "British";
      break;
    case Chinese:
      filename = "Chinese";
      break;
    case Finnish:
      filename = "Finnish";
      break;
    case French:
      filename = "French";
      break;
    case Germanic:
      filename = "Germanic";
      break;
    case Hispanic:
      filename = "Hispanic";
      break;
    case Icelandic:
      filename = "Icelandic";
      break;
    case Italian:
      filename = "Italian";
      break;
    case Japanese:
      filename = "Japanese";
      break;
    case Nordic:
      filename = "Nordic";
      break;
    case Russian:
      filename = "Russian";
      break;
    case Thai:
      filename = "Thai";
      break;
    case Vietnamese:
      filename = "Vietnamese";
      break;
    case LanguageError:
      std::cout << "Error: Uninitialised language " << language << std::endl;
      break;
    default:
      std::cout << "Error: Unrecognised language " << language << std::endl;
      break;
    }
  filename = cocos2d::FileUtils::getInstance()->fullPathForFilename("Names/" + filename + ".csv");
  std::ifstream file(filename);
  if (!file.is_open())
    {
      std::cout << "Error: Failed to open file " << filename << std::endl;
      ret.first = "Error#1";
      return ret;
    }
  std::string line;
  if (databaseIter[language] == -1)
    {
      std::getline(file, line); /// Clears the first line with the column headers
    }
  else
    {
      file.seekg(databaseIter[language]);
    }
  std::getline(file, line);
  if (file.eof())
    {
      std::cout << "Error: Ran out of names in " << filename << std::endl;
      ret.first = "Error#2";
      return ret;
    }
  databaseIter[language] = file.tellg();
  unsigned int comma1 = line.find(",");
  unsigned int comma2 = line.find(",", comma1+1);
  std::string gender = line.substr(0, comma1);
  std::string forename = line.substr(comma1+1, (comma2-1) - comma1);
  std::string surname = line.substr(comma2+1);
  if (gender.compare("male") == 0)
    ret.second = Male;
  else if (gender.compare("female") == 0)
    ret.second = Female;
  else
    std::cout << "Error: Expected gender string, got " << gender << std::endl;
  ret.first = forename + " " + surname;
  return ret;
}
std::string Names::getRandomTeamName(Language language)
{
  if (language != British)
    std::cout << "Error: Team names only programmed for British" << std::endl;
  char buffer[4] = "999";
  for (int i = 0; i != 3; i++)
    {
      int r = rand();
      r = r % 26;
      char c = 'A';
      r = static_cast<int>(c) + r;
      c = static_cast<char>(r);
      buffer[i] = c;
    }
  return buffer;
}
