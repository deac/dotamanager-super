#include "MatchOverlayScreen.h"
#include "InMatchScreen.h"
#include "../Serialise/GameSave.h"
#include <iostream>

void MatchOverlayScreen::OnEnter()
{
  Screen::OnEnter();
  Date date = GameSave::current()->getDate();
  SetWidgetText("Date", date.getString().c_str());
  SetWidgetTexture("Logo_1", GameSave::current()->getLogoName().c_str());
  //SetWidgetTexture("LogoOpponent", GameSave::current()->getLogoName().c_str());
}
void MatchOverlayScreen::TeamEvent(const char* widgetName)
{
  std::cout << "Team button" << std::endl;
  //TeamScreen* screen = new TeamScreen(GameSave::current()->getMyTeam());
  //Screen::spawn(screen, "matchteam.json");
  //screen->runReplace();
}
void MatchOverlayScreen::StrategyEvent(const char* widgetName)
{
  std::cout << "Strategy button" << std::endl;
  //Screen::spawn<StrategyScreen>("matchstrategy.json")->runReplace();
}
void MatchOverlayScreen::OpponentEvent(const char* widgetName)
{
  std::cout << "Opponent button" << std::endl;
}
void MatchOverlayScreen::PlayEvent(const char* widgetName)
{
  std::cout << "Play button" << std::endl;
  Screen::spawn<InMatchScreen>()->runReplace();
}
void MatchOverlayScreen::StatsEvent(const char* widgetName)
{
  std::cout << "Stats button" << std::endl;
}
void MatchOverlayScreen::OverviewEvent(const char* widgetName)
{
  std::cout << "Overview button" << std::endl;
}
