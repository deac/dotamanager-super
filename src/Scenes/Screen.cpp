#include "Screen.h"
#include <cocostudio/CocoStudio.h>
#include <ui/UILayout.h>
#include <ui/UITextField.h>
#include <ui/UIButton.h>
#include <ui/UIImageView.h>
USING_NS_CC;

#include <iostream>
Screen::Screen()
{
}
Scene* Screen::createScene(Screen* layer, const char* jsonFile)
{
  auto scene = Scene::create();
  layer->scene = scene;
  scene->addChild(layer);
  layer->scheduleUpdate();
  layer->addLayout(jsonFile);
  return scene;
}
int Screen::addLayout(const char* jsonFile, int _zDepth, bool swallowTouches)
{
  char buffer[64];
  sprintf(buffer,"Ui/Json/%s", jsonFile);
  auto loadedLayout = cocostudio::GUIReader::getInstance()->widgetFromJsonFile(buffer);
  if (loadedLayout == nullptr)
    {
      std::cout << "Error couldn't load " << buffer << std::endl;
      return -1;
    }
  auto castedLayout = dynamic_cast<ui::Layout*>(loadedLayout);
  if (castedLayout == nullptr)
    {
      std::cout << "Error while loading " << buffer << std::endl;
      return -1;
    }
  castedLayout->setZOrder(_zDepth);
  castedLayout->setSwallowTouches(swallowTouches);
  addChild(castedLayout);
  layouts.push_back(castedLayout);
  return layouts.size()-1;
}
void Screen::hideLayout(bool hide, int _layoutId)
{
  cocos2d::ui::Layout* layout = layouts[_layoutId];
  layout->setEnabled(!hide);
  layout->setVisible(!hide);
}
void Screen::runInitial()
{
  cocos2d::Director::getInstance()->runWithScene(scene);
}
void Screen::runReplace()
{
  cocos2d::Director::getInstance()->replaceScene(scene);
}
void Screen::spawn(Screen* screen, const char* jsonFile)
{
  createScene(screen, jsonFile);
  screen->setupEvents();
}
void Screen::onEnter()
{
  cocos2d::Layer::onEnter();
  OnEnter();
}
void Screen::update(float t)
{
  Layer::update(t);
  OnUpdate(t);
}
void Screen::genericEventFired(int _eventId)
{
  std::cout << "Generic event not handled: " << _eventId << std::endl;
}
bool Screen::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    return true;
}

bool Screen::registerEvent(EventType _type, const char* _widgetName, int _eventId, int _layoutId)
{
  auto label = GetWidget(_widgetName, _layoutId);
  if (label == 0)
    {
      return false;
    }
  eventIds[label] = _eventId;
  label->addTouchEventListener(CC_CALLBACK_2(Screen::eventListener, this));
  return true;
}
std::string Screen::GetWidgetText(const char* widgetName, int _layoutId)
{
  auto label = GetWidget(widgetName, _layoutId);
  auto textField = dynamic_cast<cocos2d::ui::TextField*>(label);
  if (textField != 0)
    {
      return textField->getString();
    }
  auto button = dynamic_cast<cocos2d::ui::Button*>(label);
  if (button != 0)
    {
      return button->getTitleText();
    }
  std::cout << "Widget named " << widgetName << " is not a TextField or a Button" << std::endl;
  return "";
}
bool Screen::SetWidgetText(const char* widgetName, const char* text, int _layoutId)
{
  auto label = GetWidget(widgetName, _layoutId);
  auto textField = dynamic_cast<cocos2d::ui::TextField*>(label);
  if (textField != 0)
    {
      textField->setString(text);
      return true;
    }
  auto button = dynamic_cast<cocos2d::ui::Button*>(label);
  if (button != 0)
    {
      button->setTitleText(text);
      return true;
    }
  std::cout << "Widget named " << widgetName << " is not a TextField or a Button" << std::endl;
  return false;
}
bool Screen::SetWidgetTexture(const char* widgetName, const char* filename, int _layoutId)
{
  cocos2d::ui::Layout* castedLayout = layouts[_layoutId];
  if (!castedLayout->isTouchEnabled())
    std::cout << "Error: touch is not enabled" << std::endl;
  /*if (!castedLayout->isFocused())
    {
      std::cout << "Error: not focused" << std::endl;
      castedLayout->setFocused(true);
      }*/
  /*if (castedLayout->isSwallowTouches())
    std::cout << "Swallows touches" << std::end;
  if (castedLayout->isPropagateTouchEvents())
  std::cout << "Propagates touches" << std::end;*/
  
  auto label = GetWidget(widgetName, _layoutId);
  auto button = dynamic_cast<cocos2d::ui::Button*>(label);
  if (button != nullptr)
    {
      char buffer[64];
      sprintf(buffer,"Ui/Json/%s", filename);
      button->loadTextureNormal(buffer);
      return true;
    }
  auto imageView = dynamic_cast<cocos2d::ui::ImageView*>(label);
  if (imageView != nullptr)
    {
      char buffer[64];
      sprintf(buffer,"Ui/Json/%s", filename);
      imageView->loadTexture(buffer);
      //imageView
      return true;
    }
  std::cout << "Widget named " << widgetName << " is not a Button or an ImageView" << std::endl;
  return false;
}
cocos2d::ui::Widget* Screen::GetWidget(const char* widgetName, int _layoutId)
{
  cocos2d::ui::Layout* layout = layouts[_layoutId];
  auto label = layout->getChildByNameRecursive(widgetName);
  if (label == nullptr)
    {
      std::cout << "Failed to find widget " << widgetName << " on " << layout->getName() << std::endl;
      return nullptr;
    }
  auto castedLabel = dynamic_cast<cocos2d::ui::Widget*>(label);
  if (castedLabel == 0)
    {
      std::cout << "Failed to cast node to widget" << std::endl;
      return nullptr;
    }
  return castedLabel;
}
void Screen::eventListener(cocos2d::Ref *pSender, cocos2d::ui::Widget::TouchEventType type)
{
  if ((int)type == 2)
    eventFired(eventIds[pSender]);
}

