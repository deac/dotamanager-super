#pragma once

#include "Screen.h"

class MainOverlayScreen : public Screen
{
 public:
  EVENTS(Strategy, PreMatch, Calender, MyTeam, News, Stats, Market, MainMenu, Mail)
  JSON_FILENAME("mainOverlay.json");
 protected:
  void OnEnter()override;
};
