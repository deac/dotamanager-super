#pragma once

#include "MainOverlayScreen.h"
#include <iostream>

class MainMenuScreen : public MainOverlayScreen
{
 public:
  //EVENTS(Strategy, PreMatch, Calender, Team, News, Stats, Market)
  //JSON_FILENAME("mainMenu.json");
 protected:
  void OnEnter()override;
  void OnUpdate(float delta)override;
};
