#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include <ui/UILayout.h>
#include <ui/UIWidget.h>

class Screen : private cocos2d::Layer
{
public:
  enum EventType
  {
    Tap
  };
  void runInitial();
  void runReplace();
  template <typename ScreenType>
  static ScreenType* spawn(const char* jsonFile)
  {
    ScreenType* screen = new ScreenType();
    createScene(screen, jsonFile);
    screen->setupEvents();
    return screen;
  }
  template <typename ScreenType>
  static ScreenType* spawn()
  {
    ScreenType* screen = new ScreenType();
    createScene(screen, ScreenType::__json_Filename);
    screen->setupEvents();
    return screen;
  }
  static void spawn(Screen* screen, const char* jsonFile);
 protected:
  Screen();
  bool registerEvent(EventType _type, const char* _widgetName, int _eventId, int _layoutId = 0);
  std::string GetWidgetText(const char* widgetName, int _layoutId = 0);
  bool SetWidgetText(const char* widgetName, const char* text, int _layoutId = 0);
  bool SetWidgetTexture(const char* widgetName, const char* filename, int _layoutId = 0);
  virtual void eventFired(int _eventId){}
  virtual void genericEventFired(int _eventId);
  virtual void OnUpdate(float t){}
  virtual void OnEnter(){}
  virtual void setupEvents(){}
  static cocos2d::Scene* createScene(Screen* layer, const char* jsonFile);
  int addLayout(const char* jsonFile, int _zDepth = 0, bool swallowTouches = false);
  void hideLayout(bool hidden, int _layoutId);
  template <typename ScreenType>
  static const char* getJsonFilename()
  {
    return ScreenType::__json_Filename;
  }
  
 private:
  cocos2d::ui::Widget* GetWidget(const char* widgetName, int _layoutId);
  void eventListener(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
  void load(const char* _resource);
  std::vector<cocos2d::ui::Layout*> layouts;
  cocos2d::Scene* scene = 0;
  // cocos2d::Layer functions
  bool init() override;
  void update(float t) override;
  void onEnter() override;
  std::map<Ref*, int> eventIds;
};

#define JSON_FILENAME(x) static constexpr const char* __json_Filename = x;

// Make a FOREACH macro
#define FE_1(WHAT, X) WHAT(X) 
#define FE_2(WHAT, X, ...) WHAT(X)FE_1(WHAT, __VA_ARGS__)
#define FE_3(WHAT, X, ...) WHAT(X)FE_2(WHAT, __VA_ARGS__)
#define FE_4(WHAT, X, ...) WHAT(X)FE_3(WHAT, __VA_ARGS__)
#define FE_5(WHAT, X, ...) WHAT(X)FE_4(WHAT, __VA_ARGS__)
#define FE_6(WHAT, X, ...) WHAT(X)FE_5(WHAT, __VA_ARGS__)
#define FE_7(WHAT, X, ...) WHAT(X)FE_6(WHAT, __VA_ARGS__)
#define FE_8(WHAT, X, ...) WHAT(X)FE_7(WHAT, __VA_ARGS__)
#define FE_9(WHAT, X, ...) WHAT(X)FE_8(WHAT, __VA_ARGS__)
#define FE_10(WHAT, X, ...) WHAT(X)FE_9(WHAT, __VA_ARGS__)
#define FE_11(WHAT, X, ...) WHAT(X)FE_10(WHAT, __VA_ARGS__)
#define FE_12(WHAT, X, ...) WHAT(X)FE_11(WHAT, __VA_ARGS__)
#define FE_13(WHAT, X, ...) WHAT(X)FE_12(WHAT, __VA_ARGS__)
#define FE_14(WHAT, X, ...) WHAT(X)FE_13(WHAT, __VA_ARGS__)
#define FE_15(WHAT, X, ...) WHAT(X)FE_14(WHAT, __VA_ARGS__)
#define FE_16(WHAT, X, ...) WHAT(X)FE_15(WHAT, __VA_ARGS__)
#define FE_17(WHAT, X, ...) WHAT(X)FE_16(WHAT, __VA_ARGS__)
#define FE_18(WHAT, X, ...) WHAT(X)FE_17(WHAT, __VA_ARGS__)
#define FE_19(WHAT, X, ...) WHAT(X)FE_18(WHAT, __VA_ARGS__)
#define FE_20(WHAT, X, ...) WHAT(X)FE_19(WHAT, __VA_ARGS__)
//... repeat as needed

#define GET_MACRO(_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15,_16,_17,_18,_19,_20,NAME,...) NAME 
#define FOR_EACH(action,...) \
  GET_MACRO(__VA_ARGS__,FE_20,FE_19,FE_18,FE_17,FE_16,FE_15,FE_14,FE_13,FE_12,FE_11,FE_10,FE_9,FE_8,FE_7,FE_6,FE_5,FE_4,FE_3,FE_2,FE_1)(action,__VA_ARGS__)


#define FUNCTION(x) void x##Event(const char* widgetName);
#define REGISTER(x) registerEvent(Tap, #x, x);
#define SWITCH(x) case x:   x##Event(widgetName);break;

#include <iostream>
#define EVENTS(Args...) FOR_EACH(FUNCTION, Args) \
  enum EventNames { Args}; \
  void RegisterEvent(const char* name, int id){std::cout << name << " " << id << "" << std::endl;} \
  void setupEvents()override\
  {\
  FOR_EACH(REGISTER, Args) \
  }\
  void eventFired(int _eventId)override	\
  {\
    const char* widgetName = "widgetName";		\
  switch(_eventId)\
    {\
  FOR_EACH(SWITCH, Args)\
 default:\
  genericEventFired(_eventId);\
  break;\
    }\
  }\

#endif // __HELLOWORLD_SCENE_H__
