#include "TeamScreen.h"
#include "PreMatchScreen.h"
#include "Strategy.h"
#include "../Serialise/GameSave.h"
#include "../Serialise/Matches/MatchDatabase.h"
#include "../Serialise/Match.h"

void PreMatchScreen::OnEnter()
{
  MatchOverlayScreen::OnEnter();
  int layout = addLayout("preMatch.json", -1);
  Match* match = MatchDatabase::current()->myNextMatchToday();

  SetWidgetText("txtOverview", match->infoString().c_str(), layout);
}
/*void PreMatchScreen::TeamEvent(const char* widgetName)
{
  TeamScreen* screen = new TeamScreen(GameSave::current()->getMyTeam());
  Screen::spawn(screen, "matchteam.json");
  screen->runReplace();
}
void PreMatchScreen::StrategyEvent(const char* widgetName)
{
  Screen::spawn<StrategyScreen>("matchstrategy.json")->runReplace();
}
void PreMatchScreen::OpponentEvent(const char* widgetName)
{
}
void PreMatchScreen::PlayEvent(const char* widgetName)
{
  std::cout << "Play button" << std::endl;
  Screen::spawn<InMatchScreen>()->runReplace();
}
*/
