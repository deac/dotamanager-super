#include "ContinueScreen.h"
#include "MainMenuScreen.h"
#include "../Serialise/Matches/MatchDatabase.h"
#include "../Serialise/Match.h"
#include "../Serialise/GameSave.h"

void ContinueScreen::ProceedEvent(const char* widgetName)
{
  std::vector<Match*> matches = MatchDatabase::current()->allMatchesToday();
  for (unsigned int i = 0; i != matches.size(); i++)
    {
      matches[i]->ResolveInstantly();
    }
  GameSave::current()->AdvanceOneDay();
  Screen::spawn<MainMenuScreen>()->runReplace();
}
