#include "InMatchScreen.h"
#include "PostMatchScreen.h"
#include "../Serialise/Matches/MatchDatabase.h"
#include "../Serialise/Match.h"

void InMatchScreen::PostMatchEvent(const char* _widgetName)
{
  MatchDatabase::current()->myNextMatchToday()->Played();
  Screen::spawn<PostMatchScreen>()->runReplace();
}
