#pragma once
#include "MainOverlayScreen.h"
class Team;

class TeamScreen: public MainOverlayScreen
{
public:
  TeamScreen(Team* _team);
protected:
  void OnEnter()override;
private:
  Team* team;
};
