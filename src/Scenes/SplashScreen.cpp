#include "SplashScreen.h"
#include "PreGameMenu.h"
#include "../Serialise/GameSave.h"

#include <iostream>

void SplashScreen::OnEnter()
{
  //std::cout << "loading" << std::endl;
}
void SplashScreen::OnUpdate(float delta)
{
  timer += delta;
  if (timer > 1.5f)
    {
      auto newScene = Screen::spawn<PreGameMenu>();
      newScene->runReplace();
    }
}
