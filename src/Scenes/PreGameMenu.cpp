#include "PreGameMenu.h"
#include "MainMenuScreen.h"
#include "NewGameMenu.h"
#include "../Serialise/GameSave.h"

void PreGameMenu::NewGameEvent(const char* widgetName)
{
  GameSave::NewGame();
  GameSave::current()->Set("ManagerName",1000);
  GameSave::SaveGameAs("mysave.save");
  
  Screen::spawn<NewGameMenu>()->runReplace();
}
void PreGameMenu::LoadGameEvent(const char* widgetName)
{
  GameSave::LoadGame("mysave.save");
  MainMenuScreen* newScene = Screen::spawn<MainMenuScreen>();
  newScene->runReplace();
}
