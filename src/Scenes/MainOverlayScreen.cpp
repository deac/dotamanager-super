#include "TeamScreen.h"
#include "MainMenuScreen.h"
#include <iostream>
#include "../Serialise/GameSave.h"
#include "../Serialise/Matches/MatchDatabase.h"
#include "Strategy.h"
#include "PreMatchScreen.h"
#include "ContinueScreen.h"
#include "Finances.h"
#include "Calender.h"
#include "Training.h"
#include "News.h"
#include "Email.h"
#include "Market.h"
#include "Mail.h"
#include "Stats.h"
#include "../Helpers/Date.h"
#include "../Helpers/Regions.h"

//#define SCENE_FUNCTION(SCENE_NAME) void MainOverlayScreen::SCENE_NAME##Event(char* widgetName){Screen::spawn<#SCENE_NAME>()->runReplace();}
#define SCENE_FUNCTION(SCENE_NAME) void MainOverlayScreen::SCENE_NAME##Event(char* widgetName){Screen::spawn<SCENE_NAME>()->runReplace;}
  
#define SCENE_BUTTONS(SCENE_FUNCTION, Args...) FOR_EACH(SCENE_FUNCTION, Args)
//SCENE_FUNCTION(Finances)
//SCENE_BUTTONS(SCENE_FUNCTION, Strategy, Continue, Finances, Calender, Training, Team, News, Email, Stats)
void TestShit();

#include <iostream>
void MainOverlayScreen::OnEnter()
{
  Screen::OnEnter();
  Date date = GameSave::current()->getDate();
  SetWidgetText("Calender", date.getString().c_str());
  std::vector<Match*> matches = MatchDatabase::current()->myUnplayedMatchesToday();
  if (matches.size() != 0)
    {
      SetWidgetText("PreMatch", "PlayMatch");
    }
  else
      SetWidgetText("PreMatch", "Continue");
  SetWidgetTexture("Logo", GameSave::current()->getLogoName().c_str());
  char buffer[12];
  sprintf(buffer, "£%d", GameSave::current()->getMoney());
  SetWidgetText("Budget", buffer);
}
/*void MainOverlayScreen::StrategyEvent(char* widgetName)
{
  std::cout << "Up event" << std::endl;
}
void MainOverlayScreen::TeamEvent(char* widgetName)
{
  std::cout << "Down event" << std::endl;
}
void MainOverlayScreen::ContinueEvent(char* widgetName)
{
  std::cout << "Left event" << std::endl;
  }*/
void MainOverlayScreen::MainMenuEvent(const char* widgetName)
{
  Screen::spawn<MainMenuScreen>()->runReplace();
}
void MainOverlayScreen::StrategyEvent(const char* widgetName)
{
  Screen::spawn<StrategyScreen>()->runReplace();
}
void MainOverlayScreen::MailEvent(const char* widgetName)
{
  Screen::spawn<MailScreen>()->runReplace();
}
void MainOverlayScreen::CalenderEvent(const char* widgetName)
{
  CalenderScreen* screen = new CalenderScreen(GameSave::current()->getDate());
  Screen::spawn(screen, Screen::getJsonFilename<CalenderScreen>());
  screen->runReplace();
}
void MainOverlayScreen::PreMatchEvent(const char* widgetName)
{
  Match* match = MatchDatabase::current()->myNextMatchToday();
  if (match == nullptr)
    Screen::spawn<ContinueScreen>()->runReplace();
  else
    Screen::spawn<PreMatchScreen>()->runReplace();
}
void MainOverlayScreen::MyTeamEvent(const char* widgetName)
{
  TeamScreen* screen = new TeamScreen(GameSave::current()->getMyTeam());
  Screen::spawn(screen, Screen::getJsonFilename<TeamScreen>());
  screen->runReplace();
}
void MainOverlayScreen::NewsEvent(const char* widgetName)
{
  Screen::spawn<NewsScreen>()->runReplace();
}
void MainOverlayScreen::MarketEvent(const char* widgetName)
{
  Screen::spawn<MarketScreen>()->runReplace();
}
void MainOverlayScreen::StatsEvent(const char* widgetName)
{
  //std::cout << "No stats screen yet" << std::endl;
  //Screen::spawn<StatsScreen>()->runReplace();
}
