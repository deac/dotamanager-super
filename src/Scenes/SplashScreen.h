#pragma once

#include "Screen.h"

class SplashScreen : public Screen
{
 public:
  JSON_FILENAME("splashScreen.json");
 protected:
  void OnEnter() override;
  void OnUpdate(float delta) override;
  float timer = 0.0f;
};
