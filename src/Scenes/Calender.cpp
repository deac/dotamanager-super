#include "Calender.h"
#include "MainMenuScreen.h"
#include "../Serialise/GameSave.h"
#include "../Serialise/Matches/MatchDatabase.h"
#include "../Serialise/Match.h"
#include "../Serialise/Team.h"
#include "../Helpers/Date.h"

#define DAY_OFFSET 11

CalenderScreen::CalenderScreen(const Date& _date)
  :date(_date)
{
}
CalenderScreen::~CalenderScreen()
{
}
void CalenderScreen::genericEventFired(int _eventId)
{
  int index = _eventId - DAY_OFFSET;
  switch (index)
    {
    case -2:
      Close();
      break;
    case -1:
      PreviousMonth();
      break;
    case 0:
      NextMonth();
      break;
    default:
      DaySelected(index- firstDay);
      break;
    }
}
#include <iostream>
void CalenderScreen::OnEnter()
{
  MainOverlayScreen::OnEnter();
  int calenderLayout = addLayout("calender.json", -1);
  dayOverlay = addLayout("dayOverlay.json", 1, true);
  hideLayout(true, dayOverlay);

  registerEvent(Tap, "MonthForward", DAY_OFFSET, calenderLayout);
  registerEvent(Tap, "MonthBackward", DAY_OFFSET-1, calenderLayout);
  registerEvent(Tap, "Close", DAY_OFFSET-2, dayOverlay);
  SetWidgetText("CalenderMonth", date.monthString(), calenderLayout);
  int days = date.daysThisMonth();
  firstDay = date.getDayOfFirstOfTheMonth();
  for (int i = 0; i != firstDay; i++)
    {
      char buffer[7];
      sprintf(buffer, "Date_%d", i);
      SetWidgetTexture(buffer, "DayNA.png", calenderLayout);
    }
  for (int i = firstDay; i != days + firstDay; i++)
    {
      char buffer[7];
      sprintf(buffer, "Date_%d", i);
      char widgetText[128];
      Date dateIter;
      dateIter.Set(i - firstDay, date.month(), date.year(), 0);
      DayString(widgetText, dateIter);
      //sprintf(widgetText, "%d\nTournament\nScrim", i + 1 - firstDay);
      SetWidgetText(buffer, widgetText, calenderLayout);
      registerEvent(Tap, buffer, i+1+DAY_OFFSET, calenderLayout);
    }
  for (int i = days + firstDay; i != 7*6; i++)
    {
      char buffer[7];
      sprintf(buffer, "Date_%d", i);
      SetWidgetTexture(buffer, "DayNA.png", calenderLayout);
    }
  std::vector<Match*> matches = MatchDatabase::current()->filter(GameSave::current()->getMyTeam());
}

void CalenderScreen::DayString(char* buffer, const Date& date)
{
  std::vector<Match*> matches = MatchDatabase::current()->filter(date, GameSave::current()->getMyTeam());
  if (matches.size() == 0)
    {
      sprintf(buffer, "%d\n", date.day()+1);
    }
  else if (matches.size() == 1)
    {
      sprintf(buffer, "%d\nMatch versus\n%s", date.day()+1, matches[0]->getOther()->getName().c_str());
    }
  else
    {
      std::stringstream ss;
      ss << date.day()+1 << std::endl;
      ss << "Matches versus:" << std::endl;
      for (unsigned int i = 0; i != matches.size()-1; i++)
	ss << matches[i]->getOther()->getName() << "," << std::endl;
      ss << matches[matches.size()-1]->getOther()->getName();
      sprintf(buffer, ss.str().c_str());
    }
}
void CalenderScreen::PreviousMonth()
{
  CalenderScreen* screen = new CalenderScreen(date.previousMonth());
  Screen::spawn(screen, Screen::getJsonFilename<CalenderScreen>());
  screen->runReplace();
}
void CalenderScreen::NextMonth()
{
  CalenderScreen* screen = new CalenderScreen(date.nextMonth());
  Screen::spawn(screen, Screen::getJsonFilename<CalenderScreen>());
  screen->runReplace();
}
void CalenderScreen::Close()
{
  hideLayout(true, dayOverlay);
}
void CalenderScreen::DaySelected(int _day)
{
  Date selected;
  selected.Set(_day-1, date.month(), date.year(), -1);
  std::vector<Match*> matches = MatchDatabase::current()->filter(selected);//, GameSave::current()->getMyTeam());
  std::string dayDetails;
  for (unsigned int i = 0; i != matches.size(); i++)
    {
      dayDetails += matches[i]->detailedString();
    }
  hideLayout(false, dayOverlay);
  SetWidgetText("TxtDate", Date::dateString(_day).c_str(), dayOverlay);
  SetWidgetText("TxtDayDetails", dayDetails.c_str(), dayOverlay);
}

