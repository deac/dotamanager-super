#include "TeamScreen.h"
#include "Market.h"
#include "Strategy.h"
#include "PreMatchScreen.h"
#include "MainMenuScreen.h"
#include "../Serialise/Team.h"
#include "../Serialise/Pro.h"
#include "../Serialise/GameSave.h"

TeamScreen::TeamScreen(Team* _team)
{
  team = _team;
}
/*void TeamScreen::MarketEvent(const char* widgetName)
{
  std::cout << "No market screen yet" << std::endl;
  //Screen::spawn<MarketScreen>()->runReplace();
}
void TeamScreen::StrategyEvent(const char* widgetName)
{
  Screen::spawn<StrategyScreen>()->runReplace();
}
void TeamScreen::MainMenuEvent(const char* widgetName)
{
  Screen::spawn<MainMenuScreen>()->runReplace();
}
void TeamScreen::PrematchEvent(const char* widgetName)
{
  Screen::spawn<PreMatchScreen>()->runReplace();
  }*/
void TeamScreen::OnEnter()
{
  MainOverlayScreen::OnEnter();
  int layout = addLayout("team.json", -1);
  SetWidgetText("TxtTeamName", GameSave::current()->getMyTeam()->getName().c_str(), layout);
  for (unsigned int i = 0; i != team->pros().size(); i++)
    {
      std::ostringstream stream;
      stream << "txtPro_" << i;// << "Name";
      //SetWidgetText(stream.str().c_str(), team->pros()[i]->getNickname().c_str());
    }
}
