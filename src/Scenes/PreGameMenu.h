#pragma once

#include "Screen.h"

class PreGameMenu: public Screen
{
 public:
  EVENTS(NewGame, LoadGame);
  JSON_FILENAME("pregame.json");
  //static constexpr char* const __json_Filename = /*const_cast<const char*> */"newgame.json";
 private:
};
