#pragma once

#include "Screen.h"

class InMatchScreen: public Screen
{
 public:
  EVENTS(PostMatch)
  JSON_FILENAME("inMatch.json")
};
