#!/bin/bash

function gen {
    echo "#pragma once" > $1.h
    echo "#include \"Screen.h\"" >> $1.h
    echo "" >> $1.h
    echo "class $1Screen: public Screen" >> $1.h
    echo "{" >> $1.h
    echo " public:" >> $1.h
    echo "  JSON_FILENAME(\"$1Screen.json\");" >> $1.h
    echo " private:" >> $1.h
    echo "};" >> $1.h
}

for file in Strategy Continue Finances Calender Training Team News Email Stats
do
    gen $file
done

