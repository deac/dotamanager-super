#pragma once

#include "Screen.h"

class MatchOverlayScreen: public Screen
{
public:
  EVENTS(Overview, Team, Strategy, Opponent, Stats, Play)
  JSON_FILENAME("matchOverlay.json");
protected:
  void OnEnter()override;
};
