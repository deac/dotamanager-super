#pragma once
#include "MainOverlayScreen.h"
#include "../Helpers/Date.h"

class CalenderScreen: public MainOverlayScreen
{
public:
  CalenderScreen(const Date& _date);
  ~CalenderScreen();
  void genericEventFired(int _eventId)override;
protected:
  void OnEnter()override;
private:
  void DayString(char* buffer, const Date& date);
  void Close();
  void NextMonth();
  void PreviousMonth();
  void DaySelected(int _day);
  Date date;
  int dayOverlay;
  int firstDay;
};
