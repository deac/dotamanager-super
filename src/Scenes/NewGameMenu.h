#pragma once

#include "Screen.h"

class NewGameMenu: public Screen
{
 public:
  EVENTS(MainMenu, LogoLeft, LogoRight)
  JSON_FILENAME("gameSetup.json");
 private:
  int logoSelection = 0;
  void SelectionChanged();
};
