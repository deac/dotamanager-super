#include "MainMenuScreen.h"
#include <iostream>
#include "../Serialise/GameSave.h"
#include "../Serialise/Matches/MatchDatabase.h"

void MainMenuScreen::OnEnter()
{
  MainOverlayScreen::OnEnter();
  addLayout("mainMenu.json", -1);
}
void MainMenuScreen::OnUpdate(float delta)
{
}
