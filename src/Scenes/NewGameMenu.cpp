#include "NewGameMenu.h"
#include "MainMenuScreen.h"
#include "../Serialise/GameSave.h"

std::string logoName(int logoSelection)
{
  char buffer[12];
  std::string letter = "A";
  unsigned int i = (int)'A';
  i += logoSelection;
  char C = (char)i;
  letter[0] = C;
  sprintf(buffer, "Team%s.png", letter.c_str());
  std::string ret = buffer;
  return ret;
}
void NewGameMenu::MainMenuEvent(const char* widgetName)
{
  std::string name = GetWidgetText("TxtEditName");
  std::string team = GetWidgetText("TxtNameTeam");
  std::string teamLogo = logoName(logoSelection);
  GameSave::current()->Update(name.c_str(), team.c_str(), teamLogo.c_str());
  Screen::spawn<MainMenuScreen>()->runReplace();
}
void NewGameMenu::LogoLeftEvent(const char* widgetName)
{
  logoSelection--;
  if (logoSelection == -1)
    logoSelection = 7;
  SelectionChanged();
}
void NewGameMenu::LogoRightEvent(const char* widgetName)
{
  logoSelection++;
  if (logoSelection == 8)
    logoSelection = 0;
  SelectionChanged();
}
void NewGameMenu::SelectionChanged()
{
  SetWidgetTexture("Logo", logoName(logoSelection).c_str());
}
