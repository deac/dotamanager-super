#pragma once

#include "Screen.h"

class PostMatchScreen: public Screen
{
public:
  EVENTS(MainMenu)
  JSON_FILENAME("postMatch.json")
};
