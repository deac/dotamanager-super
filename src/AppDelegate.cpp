#include "AppDelegate.h"
#include "Scenes/SplashScreen.h"
#include "Scenes/MainMenuScreen.h"
#include "Scenes/Screen.h"
#include "Serialise/GameSave.h"
USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
  GameSave::SaveGame();
  //std::cout << "Application exited" << std::endl;
}

void Run();
bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
  //Run();
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLViewImpl::create("My Game");
	glview->setFrameSize(1400, 720);
        director->setOpenGLView(glview);
	glview->setDesignResolutionSize(1280, 720, kResolutionShowAll);
	CCSize frameSize = glview->getFrameSize();
	//450,350
	//director->setContentScaleFactor(1280/320.0f);
    }

    // turn on display FPS
    director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    // create a scene. it's an autorelease object
    auto scene = Screen::spawn<SplashScreen>();

    // run
    scene->runInitial();

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
