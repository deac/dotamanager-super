#pragma once

class Serialiser;

class Serialisable
{
public:
  void Load(const char* filename);
  void Save(const char* filename);
  virtual ~Serialisable(){}
  virtual void Serialise(Serialiser* serialiser)=0;
protected:
private:
};

#include "Serialiser.h"
