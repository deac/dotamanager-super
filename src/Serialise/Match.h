#pragma once
#include "Serialisable.h"
#include "../Helpers/Date.h"
class Team;
class Tournament;

class Match : public Serialisable
{
 public:
  Match();
  ~Match();
  void Init(Team* _a, Team* _b, int _seriesLength, int _tournamentId, const Date& _date);
  bool hasPlayed();
  Team* getWinner();
  void ResolveInstantly();
  void Played();
  void Serialise(Serialiser* serialiser);
  Team* getA(){return a;}
  Team* getB(){return b;}
  Team* getOther(Team* _other);
  Team* getOther(); // Implied other is GameSave::current()->getMyTeam()
  const Date& getDate(){return date;}
  std::string infoString();
  std::string detailedString();
  int getTournamentId(){return tournamentId;}
private:
  Team* a;
  Team* b;
  int seriesLength;
  Date date;
  int gamesPlayed;
  int scoreA;
  int tournamentId;
  Tournament* getTournament();
};
