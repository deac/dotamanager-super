#include "Match.h"
#include "Team.h"
#include "GameSave.h"
#include "Matches/MatchDatabase.h"
#include "../Helpers/Date.h"
#include "Tournament.h"

Match::Match()
{
  gamesPlayed = scoreA = 0;
}
Match::~Match()
{
}
void Match::Init(Team* _a, Team* _b, int _seriesLength, int _tournamentId, const Date& _date)
{
  MatchDatabase::current()->AddMatch(this);
  a = _a;
  b = _b;
  seriesLength = _seriesLength;
  date.Set(_date);
  tournamentId = _tournamentId;
}

void Match::Serialise(Serialiser* serialiser)
{
  serialiser->ReferenceNode(&a);
  serialiser->ReferenceNode(&b);
  serialiser->Template(&seriesLength);
  date.Serialise(serialiser);
  serialiser->Template(&gamesPlayed);
  serialiser->Template(&scoreA);
  serialiser->Template(&tournamentId);
}

void Match::Played()
{
  gamesPlayed++;
  if (rand() % 2 == 0)
    scoreA++;
  if (!hasPlayed())
    {
      GameSave::current()->MatchCompleted(this);
    }
}
void Match::ResolveInstantly()
{
  while (!hasPlayed())
    {
      gamesPlayed++;
      if (rand() % 2 == 0)
	scoreA++;
    }
  GameSave::current()->MatchCompleted(this);
}
Team* Match::getWinner()
{
  if (!hasPlayed())
    {
      std::cout << "Error: Match has not been played yet" << std::endl;
      return nullptr;
    }
  if (scoreA > seriesLength/2)
    return a;
  return b;
}
bool Match::hasPlayed()
{
  return scoreA > seriesLength/2 || gamesPlayed-scoreA > seriesLength/2;
}
Team* Match::getOther()
{
  return getOther(GameSave::current()->getMyTeam());
}
Team* Match::getOther(Team* other)
{
  if (other == a)
    return b;
  if (other == b)
    return a;
  std::cout << "Error: getOther called by a team not in this match" << std::endl;
  return nullptr;
}

std::string Match::infoString()
{
  char buffer[128];
  sprintf(buffer, "%s v %s Game %d of %d", a->getName().c_str(), b->getName().c_str(), (gamesPlayed +1), seriesLength);
  std::string ret(buffer);
  return ret;
}
std::string Match::detailedString()
{
  std::stringstream stream;
  stream << a->getName() << " v " << b->getName() << " best of " << seriesLength << " for " << getTournament()->getName() << std::endl;
  if (gamesPlayed != 0)
    {
      stream << a->getName() << " " << scoreA << "-" << gamesPlayed-scoreA << " " << b->getName() << std::endl;
    }
  return stream.str();
}

Tournament* Match::getTournament()
{
  return GameSave::current()->getTournament(tournamentId);
}
