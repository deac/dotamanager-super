#include "GameSave.h"
#include <iostream>
#include "Pro.h"
#include "Team.h"
#include "Tournament.h"
#include "Match.h"
#include "Matches/MatchDatabase.h"
#include "Ecosystem/Ecosystem.h"
#include "Ecosystem/Region.h"
#include "../Helpers/Date.h"
#include "../Helpers/Names.h"

GameSave::GameSave()
{
}
GameSave::~GameSave()
{
  for (unsigned int i = 0; i != pros.size(); i++)
    delete pros[i];
}
void GameSave::Serialise(Serialiser* serialiser)
{
  Names::instance()->Serialise(serialiser);
  serialiser->String(&managerName);
  serialiser->String(&logoName);
  serialiser->Template(&money);
  date.Serialise(serialiser);
  serialiser->NodeArray(&pros);
  serialiser->SetReferenceFinder(&pros);
  serialiser->NodeArray(&teams);
  serialiser->SetReferenceFinder(&teams);
  serialiser->ReferenceNode(&mTeam);
  MatchDatabase::current()->Serialise(serialiser);
  Ecosystem::current()->Serialise(serialiser);
  serialiser->NodeArray(&tournaments);
  serialiser->SetReferenceFinder(&tournaments);
}
void GameSave::Update(const char* _name, const char* _teamName, const char* _logo)
{
  managerName = _name;
  mTeam->Set(_teamName);
  logoName = _logo;
}
void GameSave::Set(const char* _name, int _money)
{
  managerName = _name;
  logoName = "TeamA.png";
  money = _money;
  
  //mTeam = team;
}
int GameSave::addTournament(Tournament* _tournament)
{
  int id = tournaments.size();
  _tournament->SetId(id);
  tournaments.push_back(_tournament);
  return id;
}
void GameSave::addPro(Pro* pro)
{
  pros.push_back(pro);
}
void GameSave::addTeam(Team* team)
{
  teams.push_back(team);
}
void GameSave::StartNewGame(Country country, unsigned int teamId)
{
  date.Set(0, Date::Month::Sep, 2014, 0);

  Team* team = new Team();
  team->Set("Na'Vi");
  mTeam = Ecosystem::current()->getRegion(country)->getTeamPool()[teamId];

  Pro* pro = new Pro();
  pro->Set("Dendi", "Danil Ishutin");
  team->AddPro(pro);
  pro = new Pro();
  pro->Set("Hvost", "");
  team->AddPro(pro);
  pro = new Pro();
  pro->Set("Puppey", "");
  team->AddPro(pro);
  pro = new Pro();
  pro->Set("Kuroky", "");
  team->AddPro(pro);
  pro = new Pro();
  pro->Set("Funn1k", "");
  team->AddPro(pro);

  team = new Team();
  team->Set("Empire");

  pro = new Pro();
  pro->Set("VANSKOR", "");
  team->AddPro(pro);
  pro = new Pro();
  pro->Set("Silent", "");
  team->AddPro(pro);
  pro = new Pro();
  pro->Set("Mag", "");
  team->AddPro(pro);
  pro = new Pro();
  pro->Set("Resolution", "");
  team->AddPro(pro);
  pro = new Pro();
  pro->Set("ALWAYSWANNAFLY", "");
  team->AddPro(pro);

  for (int i = 0; i != 62; i++)
    {
      char teamName[24];
      sprintf(teamName, "Team #%d", i);
      team = new Team();
      team->Set(teamName);
      for (int ii = 0; ii != 5; ii++)
	{
	  char proName[24];
	  sprintf(proName, "Pro #%d%d", i, ii);
	  pro = new Pro();
	  pro->Set(proName, "");
	  team->AddPro(pro);
	}
    }
  Tournament* tourny = new Tournament();
  tourny->InitDefaultFormat(0, "Starladder season X");
  tourny->InviteTeams(teams);

  Ecosystem::current()->Init();  
}
GameSave* GameSave::_current = 0;
void GameSave::NewGame()
{
  if (_current != 0)
    {
      std::cout << "Error: a game is already loaded" << std::endl;
      delete _current;
    }
  _current = new GameSave();
  _current->StartNewGame(UnitedKingdom, 0);
}
void GameSave::LoadGame(const char* filename)
{
  if (_current != 0)
    {
      std::cout << "Error: a game is already loaded" << std::endl;
      delete _current;
    }
  _current = new GameSave();
  _current->filename = filename;
  _current->Load(filename);
}
void GameSave::SaveGame()
{
  if (_current == 0)
    {
      std::cout << "Error: no game currently loaded" << std::endl;
    }
  else
    {
      _current->Save(_current->filename.c_str());
    }
}
void GameSave::SaveGameAs(const char* filename)
{
  if (_current == 0)
    std::cout << "Error: no game currently loaded" << std::endl;
  else
    {
      _current->filename = filename;
      _current->Save(_current->filename.c_str());
    }
}
void GameSave::AdvanceOneDay()
{
  date++;
}
void GameSave::Print()
{
  std::cout << managerName << "," << money << mTeam->getName() << logoName << std::endl;
}
void GameSave::MatchCompleted(Match* match)
{
  getTournament(match->getTournamentId())->MatchPlayed(match);
}
Tournament* GameSave::getTournament(int _id)
{
  return tournaments[_id];
}
