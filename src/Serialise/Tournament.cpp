#include "Tournament.h"
#include "Team.h"
#include "Match.h"
#include "../Helpers/Date.h"
#include "GameSave.h"
#include <iostream>
#include <algorithm>

Tournament::Tournament()
  :name("Unnamed tournament")
{
}
Tournament::~Tournament()
{
  for (unsigned int i = 0; i != tournamentStages.size(); i++)
    delete tournamentStages[i];
}
std::string Tournament::getName()const{return name;}
void Tournament::InitDefaultFormat(int formatId, const std::string& _name)
{
  GameSave::current()->addTournament(this);
  name = _name;
  switch (formatId)
    {
    case 0:
      {
	LadderStage* stage1 = new LadderStage();
	stage1->gameFormat = 3;
	stage1->brackets = 2;
	stage1->lan = false;
	tournamentStages.push_back(stage1);//*/
	/*RoundRobinStage* stage0 = new RoundRobinStage();
	stage0->gameFormat = 2;
	stage0->groupSize = 4;
	stage0->eliminatedPerGroup = 2;
	stage0->lan = false;
	tournamentStages.push_back(stage0);//*/
	participantsRequired = 8;
	break;
      }
    case 1:
      {
	RoundRobinStage* stage0 = new RoundRobinStage();
	stage0->gameFormat = 2;
	stage0->groupSize = 4;
	stage0->eliminatedPerGroup = 2;
	stage0->lan = false;
	tournamentStages.push_back(stage0);
	LadderStage* stage1 = new LadderStage();
	stage1->gameFormat = 3;
	stage1->brackets = 2;
	stage1->lan = false;
	tournamentStages.push_back(stage1);
	SeededFinal* stage2 = new SeededFinal();
	stage2->gameFormat = 5;
	stage2->lan = false;
	tournamentStages.push_back(stage2);
	participantsRequired = 64;
	break;
      }
    default:
      std::cout << "Error: Invalid tournament format id: " << formatId << std::endl;
      break;
    }
  FormatAnalysis analysis = analyseFormat();
  if (!analysis.isValid)
    std::cout << "Error: Tournament format was not valid for " << participantsRequired << " players" << std::endl;
}
bool contains(const std::vector<Team*>& v, Team* t)
{
  for (unsigned int i = 0; i != v.size(); i++)
    if (v[i] == t)
      return true;
  return false;
}
bool contains(const std::vector<Match*>& v, Match* t)
{
  for (unsigned int i = 0; i != v.size(); i++)
    if (v[i] == t)
      return true;
  return false;
}
void remove(std::vector<Team*>& v, Team* t)
{
  for (unsigned int i = 0; i != v.size(); i++)
    if (v[i] == t)
      {
	v[i] = v.back();
	v.pop_back();
	return;
      }
  std::cout << "Error: Team is not in this array" << std::endl;
}
void Tournament::InviteTeams(const std::vector<Team*> pool)
{
  if (pool.size() - declinedTeams.size() < participantsRequired)
    {
      std::cout << "Too many teams declined the invite" << std::endl;
      return;
    }
  unsigned int potentialTeams = invitedTeams.size() + declinedTeams.size() + participants.size();
  for (unsigned int i = 0; i != pool.size(); i++)
    {
      if (potentialTeams == participantsRequired)
	{
	  return;
	}
      if (contains(invitedTeams, pool[i]) || contains(declinedTeams, pool[i]) || contains(participants, pool[i]))
	{
	}
      else
	{
	  //std::cout << "Inviting " << i << " " << potentialTeams << std::endl;
	  invitedTeams.push_back(pool[i]);
	  pool[i]->InviteToTournament(this);
	  //std::cout << "Invited" << std::endl;
	  potentialTeams++;
	}
    }
  std::cout << "No teams left to invite" << std::endl;
}
void Tournament::InviteAccepted(Team* team)
{
  if (contains(invitedTeams, team))
    {
      remove(invitedTeams, team);
      participants.push_back(team);
      if (participants.size() == participantsRequired)
	ScheduleMatches();
    }
  else
    std::cout << "Error: No pending invite for that team" << std::endl;
}
void Tournament::InviteDeclined(Team* team)
{
  std::cout << "Invite declined" << std::endl;
}
void Tournament::ScheduleMatches()
{
  if (tournamentStage != -1)
    {
      std::cout << "Error: Tournament stage expected to be -1" << std::endl;
      return;
    }
  tournamentStage = 0;
  tournamentStages[tournamentStage]->ScheduleMatches(participants, this);
}
Tournament::TournamentStage* Tournament::TournamentStage::Factory(unsigned int _id)
{
  TournamentStage* ret = nullptr;
  switch (_id)
    {
    case 0:
      ret = new RoundRobinStage();
      break;
    case 1:
      ret = new LadderStage();
      break;
    case 2:
      ret = new SeededFinal();
      break;
    default:
      std::cout << "Error: Unknown tournament stage id " << _id << std::endl;
      return ret;
    }
  if (ret->GetFactoryId() != _id)
    std::cout << "Error: Tournament stage id does not match: " << ret->GetFactoryId() << " " << _id << std::endl;
  return ret;
}
void Tournament::MatchPlayed(Match* match)
{
  if (tournamentStages[tournamentStage]->MatchPlayed(match, this))
    {
      std::cout << "Tournament stage " << tournamentStage << " ended" << std::endl;
      std::vector<Team*> progressed = tournamentStages[tournamentStage]->getProgressedTeams();
      tournamentStage++;
      if (tournamentStage == (int)tournamentStages.size())
	{
	  std::cout << progressed[0]->getName() << " won a tournament" << std::endl;
	}
      else
	{
	  tournamentStages[tournamentStage]->ScheduleMatches(progressed, this);
	}
    }
}
Match* Tournament::TournamentStage::CreateMatch(Team* a, Team* b, Tournament* tournament, const Date* date)
{
  Match* match = new Match();
  match->Init(a, b, gameFormat, tournament->id, *date);
  matches.push_back(match);
  std::cout << "Made match " << match->detailedString() << " " << date->getString() << std::endl;
  return match;
  /*if (finalMatch == nullptr || match->getDate() > finalMatch->getDate())
    {
      //std::cout << "Final match " << match->getDate()->getString() << std::endl;
      finalMatch = match;
      }*/
}
bool Tournament::RoundRobinStage::MatchPlayed(Match* match, Tournament* tournament)
{
  return match == finalMatch;
}
bool Tournament::LadderStage::MatchPlayed(Match* match, Tournament* tournament)
{
  std::cout << "Scheduling more matches" << std::endl;
  if (grandFinal->match == match)
    return true;
  if (lastScheduledMatch == match)
    ScheduleNextStage(tournament);
  return false;
}
bool Tournament::SeededFinal::MatchPlayed(Match* match, Tournament* tournament)
{
  return false;
}
void Tournament::RoundRobinStage::ScheduleMatches(const std::vector<Team*> _teams, Tournament* tournament)
{
  if (groupSize % 2 != 0)
    std::cout << "Error: No support for odd numbered round robin tournaments yet" << std::endl;
  teams = _teams;
  int gamesPerDay = 2;
  int groups = teams.size() / groupSize;
  //int matchesPerGroup = (groupSize/2)*(groupSize-1);
  Date date;
  date.Set(GameSave::current()->getDate());
  date++;
  int rounds = groupSize -1 ;
  int gamesThisDay = 0;
  // http://en.wikipedia.org/wiki/Round-robin_tournament#Scheduling_algorithm
  // This algorithm, plus having multiple groups
  //std::cout << "Groups: " << groups << " Rounds: " << rounds << " Matches per round: " << groupSize / 2 << std::endl;
  for (int round = 0; round != rounds; round++)
    {
      for (int match = 0; match != groupSize / 2; match++)
	{
	  int teamA = match;
	  if (match != 0)
	    {
	      teamA -= round;
	      if (teamA < 1)
		teamA += groupSize -1;
	    }
	  int teamB = (groupSize - 1) - match;
	  teamB -= round;
	  if (teamB < 1)
	    teamB += groupSize - 1;
	  /*
	    0 1 2 3
	    7 6 5 4
	    
	    0 7 1 2
	    6 5 4 3
	    
	    etc.
	  */
	  for (int group = 0; group != groups; group++)
	    {
	      unsigned int groupIndex = group * groupSize;
	      if (groupIndex + teamA < 0 || groupIndex + teamA >= teams.size())
		std::cout << "Error: " << groupIndex + teamA << " is out of bounds" << std::endl;
	      if (groupIndex + teamB < 0 || groupIndex + teamB >= teams.size())
		std::cout << "Error: " << groupIndex + teamB << " is out of bounds" << std::endl;
	      finalMatch = CreateMatch(teams[groupIndex + teamA], teams[groupIndex + teamB], tournament, &date);
	      gamesThisDay++;
	      if (gamesThisDay == gamesPerDay)
		{
		  date++;
		  gamesThisDay = 0;
		}
	    }
	}
    }
  /*Date dates[groups];
  for (int i = 0; i != groups; i++)
    {
      dates[i].Set(*(GameSave::current()->getDate()));
      dates[i]+= ((i/gamesPerDay)+1);
    }
  for (int i = 0; i != groups; i++)
    {
      for (int ii = i * groupSize; ii != (i+1) * groupSize; ii++)
	{
	  for (int iii = ii+1; iii != (i+1) * groupSize; iii++)
	    {
	      CreateMatch(teams[ii], teams[iii], tournament, &dates[i]);
	      dates[i]++;
	    }
	}
	}*/
}
bool sortFunction (std::pair<Team*, int> a, std::pair<Team*, int> b) { return (a.second > b.second); }
std::vector<Team*> Tournament::RoundRobinStage::getProgressedTeams()
{
  int* points = new int[teams.size()]{0};
  for (unsigned int i = 0; i != teams.size(); i++)
    if (points[i] != 0)
      std::cout << "Error: points was not initialised correctly" << std::endl;
  for (unsigned int i = 0; i != matches.size(); i++)
    {
      int teamAIndex = -1;
      int teamBIndex = -1;
      for (unsigned int ii = 0; ii != teams.size(); ii++)
	{ /// Optimize this
	  if (teams[ii] == matches[i]->getA())
	    teamAIndex = ii;
	  if (teams[ii] == matches[i]->getB())
	    teamBIndex = ii;
	}
      if (teamAIndex == -1 || teamBIndex == -1)
	  std::cout << "Error #1 in round robin logic" << std::endl;
      /// This assumes 0 points for a loss, 1 for a draw, 3 for a win
      Team* winner = matches[i]->getWinner();
      if (winner == nullptr)
	{
	  points[teamAIndex]++;
	  points[teamBIndex]++;
	}
      else if (winner == matches[i]->getA())
	{
	  points[teamAIndex] += 3;
	}
      else if (winner == matches[i]->getB())
	{
	  points[teamBIndex] += 3;
	}
      else
	{
	  std::cout << "Error #2 in round robin logic" << std::endl;
	}
    }
  std::vector<Team*> ret;
  int groups = teams.size() / groupSize;
  int progressedPerGroup = groupSize - eliminatedPerGroup;
  for (int i = 0; i != groups; i++)
    {
      int index = i*groupSize;

      std::vector<std::pair<Team*, int>> sortedPoints;
      for (int ii = index; ii != index+groupSize; ii++)
	{
	  sortedPoints.push_back({teams[ii],points[ii]});
	}
      std::sort(sortedPoints.begin(), sortedPoints.end(), sortFunction);
      for (int i = 0; i != progressedPerGroup; i++)
	{
	  ret.push_back(sortedPoints[i].first);
	}
    }
  
  delete[] points;
  return ret;
}
void Tournament::LadderStage::ScheduleMatches(const std::vector<Team*> _teams, Tournament* tournament)
{
  teams = _teams;
  Date date;
  date.Set(GameSave::current()->getDate());
  date++;
  std::vector<std::pair<int, std::vector<Scheduled*>*>> stepStack;
  int bracket = brackets;
  std::vector<Scheduled*>* current = new std::vector<Scheduled*>();
  std::vector<Scheduled*>* previous = nullptr;
  stepStack.push_back({bracket, current});
  for (unsigned int i = 0; i != teams.size(); i+=2)
    {
      current->push_back(createFrom(teams[i], teams[i+1]));
    }
  std::cout << std::endl;
  int safety = 100;
  while (stepStack.size() != 0)
    {
      safety--;
      if (safety == 0)
	{
	  std::cout << "Stack overflow" << std::endl;
	  break;
	}
      //if (bracket == brackets) // Progress up winners
	{
	  previous = current;
	  if (previous->size() == 1)
	    {
	      grandFinal = (*previous)[0];
	      stepStack.pop_back();
	    }
	  else
	    {
	      current = new std::vector<Scheduled*>();
	      stepStack.pop_back();
	      stepStack.push_back({bracket, current});
	      for (unsigned int i = 0; i != previous->size(); i+=2)
		{
		  current->push_back(createFrom((*previous)[i], true, (*previous)[i+1], true));
		}
	    }
	  delete previous;
	}
      /*else // Create losers
	{
	  previous = current;
	  }*/
    }
  std::cout << "Scheduled total of " << scheduledMatches.size() << " matches" << std::endl;
  lastScheduled = 0;//teams.size()/2;
  ScheduleNextStage(tournament);
}
void Tournament::LadderStage::ScheduleNextStage(Tournament* tournament)
{
  Date date;
  date.Set(GameSave::current()->getDate());
  date++;
  while (lastScheduled != scheduledMatches.size())
    {
      Scheduled* next = scheduledMatches[lastScheduled];
      if (next->a == nullptr)
	{
	  Scheduled* previous = scheduledMatches[next->inputA];
	  next->a = previous->getWinner();
	}
      if (next->b == nullptr)
	{
	  Scheduled* previous = scheduledMatches[next->inputB];
	  next->b = previous->getWinner();
	}
      if (next->a == nullptr || next->b == nullptr)
	{
	  return;
	}
      else
	{
	  next->match = CreateMatch(next->a, next->b, tournament, &date);
	  lastScheduledMatch = next->match;
	}
      lastScheduled++;
    }
}
Tournament::LadderStage::Scheduled::Scheduled()
{
  inputA = -1;
  inputB = -1;
  a = nullptr;
  b = nullptr;
  winnerGoes = -1;
  loserGoes = -1;
  match = nullptr;
  id = -1;
}
void Tournament::LadderStage::Scheduled::Serialise(Serialiser* serialiser)
{
  serialiser->Template(&inputA);
  serialiser->Template(&winnerOfA);
  serialiser->Template(&inputB);
  serialiser->Template(&winnerOfB);
  serialiser->ReferenceNode(&a);
  serialiser->ReferenceNode(&b);
  serialiser->Template(&winnerGoes);
  serialiser->Template(&loserGoes);
  serialiser->ReferenceNode(&match);
  serialiser->Template(&id);
}
Team* Tournament::LadderStage::Scheduled::getWinner()
{
  if (match == nullptr)
    return nullptr;
  if (!match->hasPlayed())
    return nullptr;
  return match->getWinner();
}
Tournament::LadderStage::Scheduled* Tournament::LadderStage::createFrom(Scheduled* a, bool winnerOfA, Scheduled* b, bool winnerOfB)
{
  Scheduled* ret = new Scheduled();
  scheduledMatches.push_back(ret);
  ret->id = scheduledMatches.size()-1;
  ret->inputA = a->getId();
  ret->inputB = b->getId();
  ret->winnerOfA = winnerOfA;
  ret->winnerOfB = winnerOfB;
  return ret;
}
Tournament::LadderStage::Scheduled* Tournament::LadderStage::createFrom(Team* a, Team* b)
{
  Scheduled* ret = new Scheduled();
  scheduledMatches.push_back(ret);
  ret->id = scheduledMatches.size()-1;
  if (a == nullptr || b == nullptr)
    std::cout << "Error a or b is null" << std::endl;
    
  ret->a = a;
  ret->b = b;
  return ret;
}
std::vector<Team*> Tournament::LadderStage::getProgressedTeams()
{
  std::vector<Team*> ret;
  ret.push_back(grandFinal->getWinner());
  return ret;
}
void Tournament::SeededFinal::ScheduleMatches(const std::vector<Team*> _teams, Tournament* tournament)
{
}
std::vector<Team*> Tournament::SeededFinal::getProgressedTeams()
{
  std::vector<Team*> ret;
  return ret;
}
Tournament::FormatAnalysis Tournament::analyseFormat()
{
  FormatAnalysis analysis;
  int teams = participantsRequired;
  for (unsigned int i = 0; i != tournamentStages.size(); i++)
    {
      teams = tournamentStages[i]->AnalyseFormat(teams, &analysis);
      if (teams == -1)
	{
	  analysis.isValid = false;
	  return analysis;
	}
    }
  analysis.isValid = true;
  return analysis;
}
bool Tournament::TournamentStage::SetGames(int minGames, int maxGames, FormatAnalysis* analysis)
{
  if (lan)
    {
      analysis->minLanGames += minGames;
      analysis->maxLanGames += maxGames;
    }
  else
    {
      if (analysis->minLanGames != 0 || analysis->maxLanGames != 0)
	{
	  std::cout << "Cannot have an online stage come after a lan stage" << std::endl;
	  return false;
	}
      analysis->minOnlineGames += minGames;
      analysis->maxOnlineGames += maxGames;
    }
  return true;
}
int Tournament::RoundRobinStage::AnalyseFormat(int startingTeams, FormatAnalysis* analysis)
{
  if (startingTeams % groupSize !=0)
    {
      std::cout << "Starting team size of " << startingTeams << " does not fit into groups of " << groupSize << std::endl;
      return -1;
    }
  int groups = (startingTeams / groupSize);
  int gamesPerGroup = 0;
  for (int i = groupSize; i != 1; i--)
    {
      gamesPerGroup += groupSize-1;
    }
  int seriesCount = gamesPerGroup * groups;
  int minGames = -1;
  int maxGames = -1;
  if (gameFormat % 2 == 0) // Even
    {
      maxGames = minGames = seriesCount * gameFormat;
    }
  else // Odd
    {
      maxGames = seriesCount * gameFormat;
      minGames = seriesCount * (((gameFormat-1)/2) + 1);
    }
  if (!SetGames(minGames, maxGames, analysis))
    return -1;
  return groups * (groupSize - eliminatedPerGroup);
}
int Tournament::LadderStage::AnalyseFormat(int startingTeams, FormatAnalysis* analysis)
{
  if (startingTeams % (2 * brackets) != 0)
    return -1;
  
  return brackets;
}
int Tournament::SeededFinal::AnalyseFormat(int startingTeams, FormatAnalysis* analysis)
{
  if (startingTeams == 2)
    return 1;
  return -1;
}
void Tournament::Serialise(Serialiser* serialiser)
{
  serialiser->String(&tournamentName);
  serialiser->Template(&prizePool);
  serialiser->ReferenceArray(&invitedTeams);
  serialiser->ReferenceArray(&declinedTeams);
  serialiser->ReferenceArray(&participants);
  serialiser->ReferenceArray(&eliminated);
  serialiser->ReferenceArray(&disqualified);
  serialiser->FactoryNodeArray(&tournamentStages);
  serialiser->Template(&tournamentStage);
  serialiser->Template(&id);
  serialiser->String(&name);
}
void Tournament::TournamentStage::BaseSerialise(Serialiser* serialiser)
{
  serialiser->Template(&gameFormat);
  serialiser->Template(&lan);
  serialiser->ReferenceArray(&matches);
}
void Tournament::RoundRobinStage::Serialise(Serialiser* serialiser)
{
  BaseSerialise(serialiser);
  serialiser->ReferenceNode(&finalMatch);
}
void Tournament::LadderStage::Serialise(Serialiser* serialiser)
{
  BaseSerialise(serialiser);
  serialiser->NodeArray(&scheduledMatches);
  serialiser->Template(&lastScheduled);
  serialiser->SetReferenceFinder(&scheduledMatches);
  serialiser->ReferenceNode(&grandFinal);
  serialiser->ReferenceNode(&lastScheduledMatch);
}
void Tournament::SeededFinal::Serialise(Serialiser* serialiser)
{
  BaseSerialise(serialiser);
}
