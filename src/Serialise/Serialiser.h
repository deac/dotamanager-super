#pragma once
#include <boost/type_traits.hpp>
#include <vector>
#include <fstream>
#include <cassert>
#include <iostream>
class iSerialisable;

class Serialiser
{
public:
  enum SerialiseDirection//: bool
    {
      Loading,
      Saving,
    };
  Serialiser(SerialiseDirection _direction , const char* filename);
  ~Serialiser();
  void Node(iSerialisable** node);
  void String(std::string* string);
  template <typename Type>
  void Template(Type* object);
  template <typename Type>
  void NodeArray(std::vector<Type*>* array);
  template <typename Type>
  void FactoryNodeArray(std::vector<Type*>* array);
  template <typename Type>
  void SetReferenceFinder(std::vector<Type*>* references);
  template <typename Type>
  void ReferenceNode(Type** node);
  template <typename Type>
  void ReferenceArray(std::vector<Type*>* node);
  bool isLoading(){return direction == Loading;}
private:
  template <typename Type>
  static unsigned int GetTypeId()
  {
    static unsigned int id = GetNextTypeId();
    return id;
  }
  static unsigned int GetNextTypeId()
  {
    static unsigned int id = -1;
    id++;
    return id;
  }
  SerialiseDirection direction;
  std::fstream* file;

  template <typename Type>
  Type* ResolveReference(int i);
  template <typename Type>
  int GetReference(Type* object);

  template <typename Type>
  std::vector<Type*>* GetReferenceFinder();
  std::vector<void*> referenceFinders;
};
#include "Serialisable.h"


template<typename Type>
void Serialiser::Template(Type* object)
{
  if (direction == Loading)
    {
      file->read(reinterpret_cast<char*>(object), sizeof(Type));
    }
  else
    {
      file->write(reinterpret_cast<char*>(object), sizeof(Type));
    }
}
template<typename Type>
void Serialiser::NodeArray(std::vector<Type*>* array)
{
  BOOST_STATIC_ASSERT((boost::is_base_of<Serialisable, Type>::value));
  if (direction == Loading)
    {
      unsigned int size;
      file->read(reinterpret_cast<char*>(&size), sizeof(unsigned int));
      array->clear();
      array->resize(size);
      for (unsigned int i = 0; i != size; i++)
	{
	  Type* object = new Type();
	  object->Serialise(this);
	  (*array)[i] = object;
	}
    }
  else
    {
      unsigned int size = array->size();
      file->write(reinterpret_cast<char*>(&size), sizeof(unsigned int));
      for (unsigned int i = 0; i != size; i++)
	{
	  (*array)[i]->Serialise(this);
	}
    }
}
template<typename Type>
void Serialiser::FactoryNodeArray(std::vector<Type*>* array)
{
  BOOST_STATIC_ASSERT((boost::is_base_of<Serialisable, Type>::value));
  if (direction == Loading)
    {
      unsigned int size;
      file->read(reinterpret_cast<char*>(&size), sizeof(unsigned int));
      array->clear();
      array->resize(size);
      for (unsigned int i = 0; i != size; i++)
	{
	  unsigned int id;
	  file->read(reinterpret_cast<char*>(&id), sizeof(unsigned int));
	  Type* object = Type::Factory(id);
	  object->Serialise(this);
	  (*array)[i] = object;
	}
    }
  else
    {
      unsigned int size = array->size();
      file->write(reinterpret_cast<char*>(&size), sizeof(unsigned int));
      for (unsigned int i = 0; i != size; i++)
	{
	  unsigned int id = (*array)[i]->GetFactoryId();
	  file->write(reinterpret_cast<char*>(&id), sizeof(unsigned int));
	  (*array)[i]->Serialise(this);
	}
    }
}
template <typename Type>
void Serialiser::SetReferenceFinder(std::vector<Type*>* references)
{
  BOOST_STATIC_ASSERT((boost::is_base_of<Serialisable, Type>::value));
  void* castedReferences = reinterpret_cast<void*>(references);
  unsigned int typeId = Serialiser::GetTypeId<Type>();
  if (referenceFinders.size() <= typeId)
    {
      referenceFinders.resize(typeId+1);
    }
  else
    {
      //assert(referenceFinders[typeId] == 0);
    }
  referenceFinders[typeId] = castedReferences;
}
template <typename Type>
std::vector<Type*>* Serialiser::GetReferenceFinder()
{
  unsigned int typeId = Serialiser::GetTypeId<Type>();
  assert(referenceFinders.size() > typeId);
  void* castedReferences = referenceFinders[typeId];
  assert(castedReferences != 0);
  std::vector<Type*>* references = reinterpret_cast<std::vector<Type*>*>(castedReferences);
  return references;
}
template<typename Type>
Type* Serialiser::ResolveReference(int i)
{
  BOOST_STATIC_ASSERT((boost::is_base_of<Serialisable, Type>::value));
  if (i == -1)
    return nullptr;
  std::vector<Type*>* references = GetReferenceFinder<Type>();
  Type* ret = (*references)[i];
  if (ret == 0)
    std::cout << "Reference was found to be null : " << ret << std::endl;
  return ret;
}
template <typename Type>
int Serialiser::GetReference(Type* object)
{
  BOOST_STATIC_ASSERT((boost::is_base_of<Serialisable, Type>::value));
  if (object == nullptr)
    return -1;
  std::vector<Type*>* references = GetReferenceFinder<Type>();
  for (unsigned int i = 0; i != references->size(); i++)
    {
      if ((*references)[i] == object)
	return static_cast<int>(i);
    }
  std::cout << "ERROR: Failed to find reference for " << object << std::endl;
  return 0;
}
template <typename Type>
void Serialiser::ReferenceNode(Type** object)
{
  BOOST_STATIC_ASSERT((boost::is_base_of<Serialisable, Type>::value));
  if (direction == Loading)
    {
      int index;
      file->read(reinterpret_cast<char*>(&index), sizeof(int));
      Type* reference = ResolveReference<Type>(index);
      *object = reference;
    }
  else
    {
      int index = GetReference<Type>(*object);
      file->write(reinterpret_cast<char*>(&index), sizeof(int));
    }
}
template<typename Type>
void Serialiser::ReferenceArray(std::vector<Type*>* array)
{
  BOOST_STATIC_ASSERT((boost::is_base_of<Serialisable, Type>::value));
  if (direction == Loading)
    {
      unsigned int size;
      file->read(reinterpret_cast<char*>(&size), sizeof(unsigned int));
      array->clear();
      array->resize(size);
      for (unsigned int i = 0; i != size; i++)
	{
	  unsigned short index;
	  file->read(reinterpret_cast<char*>(&index), sizeof(unsigned short));
	  Type* object = ResolveReference<Type>(index);
	  (*array)[i] = object;
	}
    }
  else
    {
      unsigned int size = array->size();
      file->write(reinterpret_cast<char*>(&size), sizeof(unsigned int));
      for (unsigned int i = 0; i != size; i++)
	{
	  Type* object = (*array)[i];
	  unsigned short index = GetReference<Type>(object);
	  file->write(reinterpret_cast<char*>(&index), sizeof(unsigned short));
	}
    }
}
