#pragma once

#include "Serialisable.h"
#include "../Helpers/Date.h"
#include "../Helpers/Gender.h"
#include "../Helpers/Country.h"

class Pro: public Serialisable
{
public:
  Pro();
  void Set(const char* _nickname, const char* _fullname);
  std::string Get(){return nickname + ", " + fullname;}
  void Serialise(Serialiser* serialiser)override;
  const std::string& getNickname(){return nickname;}
  const std::string& getFullname(){return fullname;}
private:
  std::string nickname;
  std::string fullname;
  Gender gender;
  Country country;
  Date dateOfBirth;
};
