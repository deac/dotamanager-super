#pragma once
#include "../Serialisable.h"
class Match;
class Team;
class Date;

class MatchDatabase : public Serialisable
{
public:
  static MatchDatabase* current();
  Match* myNextMatchToday();
  std::vector<Match*> myUnplayedMatchesToday();
  std::vector<Match*> myMatchesToday();
  std::vector<Match*> allMatchesToday();
  std::vector<Match*> filter(Team* team);
  std::vector<Match*> filter(const Date& date);
  std::vector<Match*> filter(const Date& date, Team* team);
  void Serialise(Serialiser* serialiser)override;
  void AddMatch(Match* match);
private:
  std::vector<Match*> matches;
};
