#include "MatchDatabase.h"
#include "../Match.h"
#include "../../Helpers/Date.h"
#include "../GameSave.h"

MatchDatabase* MatchDatabase::current()
{
  static MatchDatabase database;
  return &database;
}
void MatchDatabase::Serialise(Serialiser* serialiser)
{
  serialiser->NodeArray(&matches);
  serialiser->SetReferenceFinder(&matches);
}
void MatchDatabase::AddMatch(Match* match)
{
  matches.push_back(match);
}
Match* MatchDatabase::myNextMatchToday()
{
  std::vector<Match*> matches = myMatchesToday();
  for (unsigned int i = 0; i != matches.size(); i++)
    {
      if (!matches[i]->hasPlayed())
	return matches[i];
    }
  return nullptr;
}
std::vector<Match*> MatchDatabase::myUnplayedMatchesToday()
{
  std::vector<Match*> matches = myMatchesToday();
  std::vector<Match*> ret;
  for (unsigned int i = 0; i != matches.size(); i++)
    {
      if (!matches[i]->hasPlayed())
	ret.push_back(matches[i]);
    }
  return ret;
}
std::vector<Match*> MatchDatabase::myMatchesToday()
{
  return MatchDatabase::current()->filter(GameSave::current()->getDate(), GameSave::current()->getMyTeam());
}
std::vector<Match*> MatchDatabase::allMatchesToday()
{
  return MatchDatabase::current()->filter(GameSave::current()->getDate());
}
std::vector<Match*> MatchDatabase::filter(Team* team)
{
  std::vector<Match*> ret;
  for (unsigned int i = 0; i != matches.size(); i++)
    {
      if (matches[i]->getA() == team || matches[i]->getB() == team)
	ret.push_back(matches[i]);
    }
  return ret;
}
std::vector<Match*> MatchDatabase::filter(const Date& date)
{
  std::vector<Match*> ret;
  for (unsigned int i = 0; i != matches.size(); i++)
    {
      if (matches[i]->getDate().Equals(date))
	ret.push_back(matches[i]);
    }
  return ret;
}
std::vector<Match*> MatchDatabase::filter(const Date& date, Team* team)
{
  std::vector<Match*> input = filter(team);
  std::vector<Match*> ret;
  for (unsigned int i = 0; i != input.size(); i++)
    {
      if (input[i]->getDate().Equals(date))
	ret.push_back(input[i]);
    }
  return ret;
}
