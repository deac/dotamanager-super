#include "Pro.h"
#include "GameSave.h"

Pro::Pro()
{
}

void Pro::Serialise(Serialiser* serialiser)
{
  serialiser->String(&nickname);
  serialiser->String(&fullname);
}

void Pro::Set(const char* _nickname, const char* _fullname)
{
  GameSave::current()->addPro(this);
  nickname = _nickname;
  fullname = _fullname;
}
