#pragma once

#include "Serialisable.h"
#include <vector>
class Pro;
class Tournament;

class Team : public Serialisable
{
 public:
  void Serialise(Serialiser* serialiser)override;
  void Set(const char* _teamName);
  void AddPro(Pro* pro){_pros.push_back(pro);}
  void InviteToTournament(Tournament* tournament);
  std::string getName();
  const std::vector<Pro*>& pros(){return _pros;}
 private:
  std::string teamName;
  std::vector<Pro*> _pros;
};
