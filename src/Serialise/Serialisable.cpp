#include "Serialisable.h"

void serialise(Serialisable* object, Serialiser::SerialiseDirection _direction, const char* filename)
{
  Serialiser* serialiser = new Serialiser(_direction, filename);
  object->Serialise(serialiser);
  delete serialiser;
}
void Serialisable::Load(const char* filename)
{
  serialise(this, Serialiser::Loading, filename);
}
void Serialisable::Save(const char* filename)
{
  serialise(this, Serialiser::Saving, filename);
}
