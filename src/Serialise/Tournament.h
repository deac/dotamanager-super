#pragma once


#include "Serialisable.h"
class CalenderScreen;
class Team;
class Match;
class Date;

class Tournament : public Serialisable
{
public:
  Tournament();
  ~Tournament();
  void InitDefaultFormat(int formatId, const std::string& _name);
  void InviteTeams(const std::vector<Team*> pool);
  void InviteAccepted(Team* team);
  void InviteDeclined(Team* team);
  void Serialise(Serialiser* serialiser)override;
  void FillTeamCalender(Team* team, CalenderScreen* calender);
  void MatchPlayed(Match* match);
  std::string getName()const;
  void SetId(int _id){id = _id;}
private:
  void ScheduleMatches();
  std::string tournamentName;
  int prizePool;
  std::vector<Team*> invitedTeams; // Pending invitations only
  std::vector<Team*> declinedTeams;
  std::vector<Team*> participants;
  std::vector<Team*> eliminated;
  std::vector<Team*> disqualified;
  const std::vector<Team*>& remaining();
  struct FormatAnalysis;
  class TournamentStage : public Serialisable
  {
  public:
    virtual ~TournamentStage(){}
    static TournamentStage* Factory(unsigned int _id);
    virtual unsigned int GetFactoryId()=0;
    virtual int AnalyseFormat(int startingTeams, FormatAnalysis* analysis)=0;
    virtual void ScheduleMatches(const std::vector<Team*> _teams, Tournament* tournament)=0;
    virtual std::vector<Team*> getProgressedTeams()=0;
    int gameFormat = 1; // Bo1, Bo2, etc.
    bool lan = false;
    virtual bool MatchPlayed(Match* match, Tournament* tournament)=0;// Returns true if this stage of the tournament is over
  protected:
    void BaseSerialise(Serialiser* serialiser);
    bool SetGames(int minGames, int maxGames, FormatAnalysis* analysis);
    Match* CreateMatch(Team* a, Team* b, Tournament* tournament, const Date* _date);
    std::vector<Team*> teams;
    std::vector<Match*> matches;
  private:
    unsigned int factoryId = -1;
  };
  class RoundRobinStage : public TournamentStage
  {
  public:
    unsigned int GetFactoryId()override{return 0;}
    int groupSize = 4; // How many teams per group, -1 indicates all teams in one group
    int eliminatedPerGroup = 2;
    int AnalyseFormat(int startingTeams, FormatAnalysis* analysis)override;
    void ScheduleMatches(const std::vector<Team*> _teams, Tournament* tournament);
    std::vector<Team*> getProgressedTeams()override;
    void Serialise(Serialiser* serialiser)override;
    bool MatchPlayed(Match* match, Tournament* tournament)override;// Returns true if this stage of the tournament is over
  private:
    Match* finalMatch;
  };
  class LadderStage : public TournamentStage
  {
  public:
    unsigned int GetFactoryId()override{return 1;}
    int brackets = 1; // Single elim, double elim
    int AnalyseFormat(int startingTeams, FormatAnalysis* analysis)override;
    void ScheduleMatches(const std::vector<Team*> _teams, Tournament* tournament);
    std::vector<Team*> getProgressedTeams()override;
    void Serialise(Serialiser* serialiser)override;
    bool MatchPlayed(Match* match, Tournament* tournament)override;// Returns true if this stage of the tournament is over
  private:
    class Scheduled : public Serialisable
    {
    public:
      Scheduled();
      int inputA;
      bool winnerOfA;
      int inputB;
      bool winnerOfB;
      Team* a;
      Team* b;
      int winnerGoes;
      int loserGoes;
      int getId(){return id;}
      Match* match;
      Team* getWinner();
      void Serialise(Serialiser* serialiser)override;
      int id;
    private:
    };
    std::vector<Scheduled*> scheduledMatches;
    Scheduled* createFrom(Scheduled* a, bool winnerOfA, Scheduled* b, bool winnerOfB);
    Scheduled* createFrom(Team* a, Team* b);
    void ScheduleNextStage(Tournament* tournament);
    unsigned int lastScheduled;
    Scheduled* grandFinal;
    Match* lastScheduledMatch;
  };
  class SeededFinal : public TournamentStage // One game advantage for a team
  {
  public:
    unsigned int GetFactoryId()override{return 2;}
    int AnalyseFormat(int startingTeams, FormatAnalysis* analysis)override;
    void ScheduleMatches(const std::vector<Team*> teams, Tournament* tournament);
    std::vector<Team*> getProgressedTeams()override;
    void Serialise(Serialiser* serialiser)override;
    bool MatchPlayed(Match* match, Tournament* tournament)override;// Returns true if this stage of the tournament is over
  };
  struct FormatAnalysis
  {
    bool isValid = false;
    int participantsRequired = 0;
    int minOnlineGames = 0;
    int maxOnlineGames = 0;
    int minLanGames = 0;
    int maxLanGames = 0;
  };
  FormatAnalysis analyseFormat();
  int tournamentStage = -1; // Which stage of the tournament is currently active, -1 indicates invites are still being sent out
  std::vector<TournamentStage*> tournamentStages;
  unsigned int participantsRequired; // When this goal is reached the tournament may begin
  int id;
  std::string name;
};
