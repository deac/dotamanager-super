#include "Team.h"
#include "Pro.h"
#include "Tournament.h"
#include "GameSave.h"

void Team::Set(const char* _teamName)
{
  GameSave::current()->addTeam(this);
  teamName = _teamName;
}

void Team::Serialise(Serialiser* serialiser)
{
  serialiser->String(&teamName);
  serialiser->ReferenceArray(&_pros);
}

std::string Team::getName()
{
  if (teamName == std::string(""))
    std::cout << "Empty team name: " << this << std::endl;
  return teamName;
}

void Team::InviteToTournament(Tournament* tournament)
{
  tournament->InviteAccepted(this);
}
