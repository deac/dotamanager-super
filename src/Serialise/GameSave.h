#pragma once

#include "Serialisable.h"
#include "../Helpers/Date.h"
#include "../Helpers/Country.h"
class Pro;
class Team;
class Tournament;
class Match;

class GameSave : public Serialisable
{
public:
  void Set(const char* _name, int _money);
  void Update(const char* _name, const char* _teamName, const char* _logo);
  void Print();
  void Serialise(Serialiser* serialiser)override;
  static GameSave* current(){return _current;}
  static void NewGame();
  static void LoadGame(const char* filename);
  static void SaveGame();
  static void SaveGameAs(const char* filename);
  Team* getMyTeam(){return mTeam;}
  const Date& getDate(){return date;}
  void AdvanceOneDay();
  void MatchCompleted(Match* match);
  const std::string& getLogoName()const{return logoName;}
  Tournament* getTournament(int _tournamentId);
  int addTournament(Tournament* tournament);
  void addPro(Pro* pro);
  void addTeam(Team* team);
  int getMoney(){return money;}
private:
  void StartNewGame(Country country, unsigned int teamId);
  GameSave();
  ~GameSave()override;
  std::string filename;
  std::string managerName;
  std::string logoName;
  int money;
  Date date;
  Team* mTeam;
  std::vector<Pro*> pros;
  std::vector<Pro*> freeAgents;
  std::vector<Team*> teams;
  std::vector<Tournament*> tournaments;
  static GameSave* _current;
};
