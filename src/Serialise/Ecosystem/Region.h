#pragma once

#include "../Serialisable.h"
#include "../../Helpers/Country.h"
#include <vector>
class TournamentOrganiser;
class Team;
class Pro;

class Region : Serialisable
{
public:
  Region();
  ~Region();
  void Init();
  void Serialise(Serialiser* serialiser)override;
  virtual std::vector<Team*> getTeamPool()=0;
protected:
  std::vector<TournamentOrganiser*> tournamentOrganisers;
  virtual std::vector<Pro*> getFreeAgentPool()=0;
};
