#pragma once

#include "../Region.h"

class GlobalRegion : public Region
{
public:
  void Serialise(Serialiser* serialiser)override;
private:
  void Init();
  TournamentOrganiser* theInternational()const{return tournamentOrganisers[0];}
  std::vector<Team*> getTeamPool()override;
  std::vector<Pro*> getFreeAgentPool()override;
};
