#include "CountryRegion.h"
#include "../Tournaments/RegionalAmateurLeague.h"
#include "../../Team.h"
#include "../../Pro.h"
#include "../../../Helpers/Names.h"

CountryRegion::CountryRegion()
{
}

CountryRegion::~CountryRegion()
{
}
void CountryRegion::Init(Country _country)
{
  Region::Init();
  country = _country;
  RegionalAmateurLeague* league = new RegionalAmateurLeague();
  league->Init(country);
  league->scheduleNext();
  tournamentOrganisers.push_back(league);
}
void CountryRegion::Serialise(Serialiser* serialiser)
{
  Region::Serialise(serialiser);
  serialiser->Template(&country);
  serialiser->ReferenceArray(&teams);
  serialiser->ReferenceArray(&freeAgents);
  serialiser->Template(&teamSeed);
}
std::vector<Team*> CountryRegion::getTeamPool()
{
  if (teams.size() == 0)
    {
      for (int i = 0; i != 100; i++)
	{
	  teams.push_back(GenerateTeam());
	}
    }
  return teams;
}
std::vector<Pro*> CountryRegion::getFreeAgentPool()
{
  if (freeAgents.size() == 0)
    {
      for (int i = 0; i != 50; i++)
	{
	  freeAgents.push_back(GeneratePlayer());
	}
    }
  return freeAgents;
}
Team* CountryRegion::GenerateTeam()
{
  Team* team = new Team();
  team->Set(Names::instance()->getRandomTeamName(British).c_str());
  for (int i = 0; i != 5; i++)
    {
      Pro* pro = GeneratePlayer();
      team->AddPro(pro);
    }
  return team;
}
Pro* CountryRegion::GeneratePlayer()
{
  Pro* pro = new Pro();
  pro->Set("", Names::instance()->getRandomName(British).first.c_str());
  return pro;
}
