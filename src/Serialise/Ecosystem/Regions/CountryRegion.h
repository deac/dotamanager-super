#pragma once

#include "../Region.h"
#include "../../../Helpers/Country.h"
#include <vector>
class TournamentOrganiser;

class CountryRegion : public Region
{
public:
  CountryRegion();
  ~CountryRegion();
  void Init(Country _country);
  void Serialise(Serialiser* serialiser)override;
protected:
  void Init();
  Country country;
  std::vector<Team*> getTeamPool()override;
  std::vector<Pro*> getFreeAgentPool()override;
  Team* GenerateTeam();
  Pro* GeneratePlayer();
  std::vector<Team*> teams;
  std::vector<Pro*> freeAgents;
  unsigned int teamSeed;
};
