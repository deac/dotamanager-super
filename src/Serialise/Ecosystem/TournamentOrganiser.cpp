#include "Tournaments/TheInternational.h"
#include "Tournaments/RegionalAmateurLeague.h"

/*void TournamentOrganiser::Init()
{
}*/
void TournamentOrganiser::Serialise(Serialiser* serialiser)
{
}

TournamentOrganiser* TournamentOrganiser::Factory(unsigned int _id)
{
  TournamentOrganiser* ret = nullptr;
  switch (_id)
    {
    case 0:
      ret = new TheInternational();
      break;
    case 1:
      ret = new RegionalAmateurLeague();
      break;
    }
  if (ret->GetFactoryId() != _id)
    std::cout << "Error: Tournament organiser id does not match: " << ret->GetFactoryId() << " " << _id << std::endl;
  return ret;
}
unsigned int TournamentOrganiser::GetFactoryId()
{
  return mType;
}
