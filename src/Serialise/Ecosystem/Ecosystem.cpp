#include "Ecosystem.h"
#include "Regions/GlobalRegion.h"
#include "Regions/CountryRegion.h"
#include <iostream>

Ecosystem::Ecosystem()
{
  global = new GlobalRegion();
  britain = new CountryRegion();
}
Ecosystem::~Ecosystem()
{
  delete global;
  delete britain;
}
Ecosystem* Ecosystem::current()
{
  static Ecosystem instance;
  return &instance;
}
void Ecosystem::Init()
{
  global->Init();
  britain->Init(UnitedKingdom);
}
void Ecosystem::Serialise(Serialiser* serialiser)
{
  global->Serialise(serialiser);
  britain->Serialise(serialiser);
}
Region* Ecosystem::getRegion(Country _country)
{
  if (_country != UnitedKingdom)
    std::cout << "This logic not yet implemented in Ecosystem.cpp" << std::endl;
  return britain;
}
