#include "RegionalAmateurLeague.h"
#include "../../Tournament.h"
#include "../Ecosystem.h"
#include "../Region.h"

RegionalAmateurLeague::RegionalAmateurLeague()
  :TournamentOrganiser(RegionalAmateurLeagueType)
{
}

void RegionalAmateurLeague::Init(Country _country)
{
  country = _country;
}

Tournament* RegionalAmateurLeague::scheduleNext()
{
  Tournament* tournament = new Tournament();
  tournament->InitDefaultFormat(0, "Regional Amateur League");
  tournament->InviteTeams(Ecosystem::current()->getRegion(country)->getTeamPool());
  return tournament;
}
