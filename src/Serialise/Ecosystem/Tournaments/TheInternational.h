#pragma once
#include "../TournamentOrganiser.h"

class TheInternational : public TournamentOrganiser
{
 public:
  TheInternational();
  ~TheInternational();
  void Init();
  Tournament* scheduleNext()override;
  void Serialise(Serialiser* serialiser)override;
};
