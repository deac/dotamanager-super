#pragma once
#include "../../../Helpers/Country.h"
#include "../TournamentOrganiser.h"

class RegionalAmateurLeague : public TournamentOrganiser
{
public:
  RegionalAmateurLeague();
  void Init(Country _country);
  Tournament* scheduleNext();
private:
  Country country;
};
