#pragma once

#include "../Serialisable.h"
class Tournament;

class TournamentOrganiser : Serialisable
{
public:
  //void Init();
  void Serialise(Serialiser* serialiser)override;
  virtual Tournament* scheduleNext()=0;
  static TournamentOrganiser* Factory(unsigned int _id);
  unsigned int GetFactoryId();
protected:
  enum Type
    {
      TheInternationalType,
      RegionalAmateurLeagueType,
      TypesSize,
    };
  Type mType;
  TournamentOrganiser(Type _type){mType = _type;}
};
