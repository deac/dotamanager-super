#pragma once

#include "../Serialisable.h"
#include "../../Helpers/Country.h"

class Region;
class CountryRegion;

class Ecosystem : Serialisable
{
public:
  Ecosystem();
  ~Ecosystem();
  static Ecosystem* current();
  void Init();
  void Serialise(Serialiser* serialiser)override;
  Region* getGlobal(){return global;}
  Region* getRegion(Country _country);
private:
  Region* global;
  CountryRegion* britain;
};
