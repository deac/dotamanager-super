#include "Serialiser.h"

Serialiser::Serialiser(SerialiseDirection _direction, const char* filename)
{
  direction = _direction;
  if (direction == Loading)
    file = new std::fstream(filename, std::ios_base::in);
  else
    file = new std::fstream(filename, std::ios_base::out);
}

Serialiser::~Serialiser()
{
  delete file;
}

void Serialiser::String(std::string* string)
{
  if (direction == Loading)
    {
    }
  else
    {
    }
  if (direction == Loading)
    {
      unsigned int size = 0;
      file->read(reinterpret_cast<char*>(&size), sizeof(unsigned int));
      char buffer[size+1];
      file->read(buffer, size);
      buffer[size] = '\0';
      *string = buffer;
    }
  else
    {
      unsigned int size = string->size();
      file->write(reinterpret_cast<char*>(&size), sizeof(unsigned int));
      file->write(&(*string)[0], size);
    }
}
