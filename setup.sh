#! /bin/bash

git submodule init
git submodule update
cd cocos2d
python2 setup.py
python2 download-deps.py
# If on linux, you may need to build/install-deps-linux.sh
cd ..
# Run cmake or open visual studio project

