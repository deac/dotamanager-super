#include <cassert>
#include <iostream>
#include <vector>

class Relay;
class BaseInterface
{
protected:
  virtual void* Call(void* _params)=0;
  template <typename RelayType>
  RelayType* _getRelay()
  {
    assert(dynamic_cast<RelayType*>(relay));
    return static_cast<RelayType*>(relay);
  }
  BaseInterface(Relay* _relay){relay = _relay;}
private:
  BaseInterface();
  BaseInterface(const BaseInterface& rhs);
  Relay* relay;
};
template <typename RelayType>
class Interface : private BaseInterface
{
protected:
  friend class Relay;
  Interface(RelayType* _relay):BaseInterface(_relay){}
  RelayType* getRelay()
  {
    return _getRelay<RelayType>();
  }
  
};
template <typename RelayType>
class Interface2 : protected Interface<RelayType>
{
protected:
  friend class Relay;
  Interface2(RelayType* _relay):Interface<RelayType>(_relay){}
  //void Call(typename RelayType::Function f);
  typename RelayType::FunctionPointer func = nullptr;
  virtual void* Call(void* _params)override
  {
    if (func == nullptr)
      std::cout << "Error unimplemented function" << std::endl;
    return nullptr;
  }
};

class Relay
{
public:
  virtual ~Relay();
protected:
  template <typename RelayType>
  void _Add(Interface2<RelayType>* _trigger) // Add a trigger
  {
    interfaces.push_back(_trigger);
  }
  void _Remove(BaseInterface* _trigger);
private:
  std::vector<BaseInterface*> interfaces;
};

template <typename RelayType>
class GetRelay;
class VoidFunctionoid;
class VoidRelay: public Relay
{
public:
  VoidRelay();
  typedef void Function (void*);
  //VoidRelay(Function* _function);

  //typedef void CheatFunction(void*);
  typedef void (*FunctionPointer)();
  //Function Call;

  void Call();
  //Interface2<VoidRelay>* operator&();
  //void Add (Interface2<VoidRelay>* relay);

  template <typename ObjectType>
  void Add(ObjectType* _object, void(ObjectType::*_functionPtr)());
  void Add(VoidRelay* _relay);
  template <typename ObjectType>
  void SafeCall();
  template <typename RelayType>
  Interface<GetRelay<RelayType>>* operator()();
private:
  void* object;
  VoidFunctionoid* functionPtr;

  VoidRelay* secondRelay;
};

class VoidInterface: public Interface2<VoidRelay>
{
public:
  VoidInterface(VoidRelay* _relay):Interface2<VoidRelay>(_relay){}
protected:
  void* Call(void* _params)override
  {
    if (_params != nullptr)
      std::cout << "Logic error" << std::endl;
    getRelay()->Call();
    return nullptr;
  }
};

Relay::~Relay()
{
}

/*Interface2<VoidRelay>* VoidRelay::operator&()
{
  return new VoidInterface(this);
}
void VoidRelay::Add(Interface2<VoidRelay>* relay)
{
  _Add(relay);
}
*/
class Door
{
public:
  Door();
  VoidRelay Open;
  VoidRelay Close;
private:
  void OpenFunction();
  void CloseFunction();
};
class Light
{
public:
  Light();
  VoidRelay On;
  VoidRelay Off;
private:
  void OnFunction();
  void OffFunction();
};


