

/*class RelayBase
{
public:
  void Add(Relay& _trigger); // Add a trigger
  void Call(ParameterList _params);
  //FunctionPointer<paramters>& const operator*(); // Set the trigger
protected:
  virtual void CallFunction(ParamterList _params);
private:
  friend class Relay;
  RelayBase(){me = this;}
  RelayBase(RelayBase* _me){me = _me;}
  FunctionPointer operator&()const; // Get the event (
  FunctionPointer operator*()const; // Get the trigger (
  const FunctionPointer<parameters> eventCache;
  FunctionPointer<parameters> trigger;
  RelayBase* me;
};
class Relay : private RelayBase
{
public:
  Relay():RelayBase(this){}
  ~Relay()
  {
    if (me != this)
      {
	assert(dynamic_cast<VirtualRelay*>(me));
	delete static_cast<VirtualRelay*>(me); /// nullptr in 90% of cases
      }
  }
  void Call(ParameterList _params);
private:
};
  class VirtualRelay: public RelayBase
  {
  public:
    virtual ~VirtualRelay(){}
  };
*/
#include "Relay.h"

void Run()
{
  Door door;
  
  Light light;

  door.Open.Add(&light.On); // When the door opens the light turns on
  door.Close.Add(&light.Off); // When the door closes the light turns off
  //light.On.Add(door.Open); // Circular references can happen

  door.Open.Call(); /// This should call door.OpenFunction() and then light.OnFunction()
  door.Close.Call();
}

Door::Door()
{
  void (Door::*openFunction)() = &Door::OpenFunction;
  Open.Add(this, openFunction);
  Close.Add(this, &Door::CloseFunction);
}
void Door::OpenFunction()
{
  std::cout << "Door opened" << std::endl;
}
void Door::CloseFunction()
{
  std::cout << "Door closed" << std::endl;
}
Light::Light()
{
  void (Light::*onFunction)() = &Light::OnFunction;
  On.Add(this, onFunction);
  Off.Add(this, &Light::OffFunction);
}
void Light::OnFunction()
{
  std::cout << "Light turned on" << std::endl;
}
void Light::OffFunction()
{
  std::cout << "Light turned off" << std::endl;
}

class VoidFunctionoid
{
public:
  virtual void Call(void* _this)=0;
};
template <typename ObjectType>
class TVoidFunctionoid : public VoidFunctionoid
{
public:
  TVoidFunctionoid(void(ObjectType::*_functionPtr)()){functionPtr = _functionPtr;}
  virtual void Call(void* _this)
  {
    ObjectType* _this2 = static_cast<ObjectType*>(_this);
    (_this2->*functionPtr)(
);
  }
private:
  void(ObjectType::*functionPtr)();
};
template <typename ObjectType>
void VoidRelay::Add(ObjectType* _object, void(ObjectType::*_functionPtr)())
{
  object = static_cast<void*>(_object);
  functionPtr = new TVoidFunctionoid<ObjectType>(_functionPtr);
  //
}
VoidRelay::VoidRelay()
{
  secondRelay = nullptr;
}
void VoidRelay::Call()
{
  functionPtr->Call(static_cast<void*>(object));
  if (secondRelay != nullptr)
    secondRelay->Call();
}
void VoidRelay::Add(VoidRelay* _relay)
{
  secondRelay = _relay;
}
template <typename ObjectType>
void VoidRelay::SafeCall()
{
  ObjectType* _object = reinterpret_cast<ObjectType*>(object);
}
//home/alasdair/DotaManager/MyGame/Final/Relay.cpp:81:22: warning: converting from 'void (Door::*)()' to 'void*' [-Wpmf-conversions]

/*template <typename Type>
class GetterRelay: VoidRelay
{
public:
  const Type& Get();
  
  Interface<GetRelay<Type>>* operator()();
};

template <typename Type>
class SetterRelay
{
public:
  void Set(const Type* _value);
  
};

template <typename Type>
class InOut: SetterRelay<Type>
{
public:
  const Type& Call(const Type& _input);
};
*/
