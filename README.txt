To compile:
Run setup.sh (interactive)
    - Requires python2, cmake, git and github access
Then run make or whatever build tool you want to use

To create and build this project from scratch:

cocos new -p MyCppGame -l cpp -d MyCppGame
cd MyCppGame/MyCppGame
rm -r Classes
rm -r Resources
rm -r cocos2d
rm CMakeLists.txt
cp -r . ../..
cd ../..
rm -r MyCppGame
git submodule init
git submodule update
cd cocos2d
python2 setup.py
python2 download-deps.py #this script requires interaction
# If on linux, you may need to run build/install-deps-linux.sh just once
cd ..
cmake .
make -j 4


